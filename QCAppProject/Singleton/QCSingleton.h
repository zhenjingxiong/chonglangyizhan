//
//  QCSingleton.h
//  QCAppProject
//
//  Created by yjl on 2021/8/19.
//

#import <Foundation/Foundation.h>
#import "QCURL.h"
#import "QCUserInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface QCSingleton : NSObject


/**
 描述：网络工具
 */
@property (nonatomic, strong) QCURL *URL;

/**
 描述：account 账号
 */
@property(nonatomic,copy) NSString * account;
@property(nonatomic,strong) QCUserInfo * userInfo;

+ (QCSingleton *)instance;

@end

NS_ASSUME_NONNULL_END
