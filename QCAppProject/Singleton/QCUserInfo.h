//
//  QCUserInfo.h
//  QCAppProject
//
//  Created by yjl on 2021/8/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QCUserInfo : NSObject


//账号
@property(nonatomic,copy)NSString * account;


@end

NS_ASSUME_NONNULL_END
