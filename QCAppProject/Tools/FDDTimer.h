//
//  FDDTimer.h
//  FDDPlus
//
//  Created by Fadada-Zhou on 2017/4/13.
//  Copyright © 2017年 fadada. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    TEXT = 1, //短信
    VOICE = 2,   //语音
}SmsKindType;


@class FDDTimer;
@protocol FDDTimerDelegate <NSObject>

-(void)willFDDTimerStart:(FDDTimer *) timer;
-(void)didFDDTimerStart:(FDDTimer *) timer seconds:(NSInteger)seconds;
-(void)didFDDTimerStop:(FDDTimer *) timer;

@end

@interface FDDTimer : NSObject

@property (nonatomic, weak) id <FDDTimerDelegate> delegate;
@property (nonatomic, assign) SmsKindType   type;
@property (nonatomic, assign) NSInteger         secondsCountDown;

-(instancetype)initWithSecoundCount:(NSInteger)seconds type:(SmsKindType)type;
-(void)startFDDTimer;
-(void)stopFDDTimer;

@end
