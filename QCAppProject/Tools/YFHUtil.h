//
//  YFHUtil.h
//  yfhmall
//
//  Created by mac1 on 2017/7/21.
//  Copyright © 2017年 dwq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YFHUtil : NSObject
//判断字符串是空字符串
+ (BOOL)isBlankString:(NSString *)string;

@end
