//
//  QCUtil.h
//  QCAppProject
//
//  Created by yjl on 2021/8/16.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QCUtil : NSObject

//判断字符串是空字符串
+ (BOOL)isBlankString:(NSString *)string;

+ (UIView *)getSeparator:(UIColor *)color frame:(CGRect)frame;

+ (NSString *)copyDataBaseWithFileFullName:(NSString *)name CustomerDir:(NSString *)dir Override:(BOOL)ovrid;


@end

NS_ASSUME_NONNULL_END
