//
//  QCUtil.m
//  QCAppProject
//
//  Created by yjl on 2021/8/16.
//

#import "QCUtil.h"

@implementation QCUtil

#pragma mark -
#pragma mark string is blank
+ (BOOL)isBlankString:(NSString *)string
{
    if (string == nil || string == NULL) {
        return YES;
    }
    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([string isEqualToString:@"(null)"] || [string isEqualToString:@"<null>"] ||[string isEqualToString:@"null"] ) {
        return YES;
    }
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    return NO;
}



+ (UIView *)getSeparator:(UIColor *)color frame:(CGRect)frame
{
    
    UIView *separatorView = [[UIView alloc] initWithFrame:frame];
    [separatorView setBackgroundColor:color];
    return separatorView;
    
}

/**
 *  @brief  Copy sqlite file
 *
 *  @param path Copy to destination location
 *
 *  @return Copy result
 *
 *  @since 1.0.0
 */
+ (NSString *)copyDataBaseWithFileFullName:(NSString *)name CustomerDir:(NSString *)dir Override:(BOOL)ovrid
{
    
    BOOL success;
    NSError *error;
    NSFileManager *fm = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath;
    if (dir && ![self isBlankString:dir]) {
        
        NSString *subDir = [NSString stringWithFormat:@"%@/%@", documentsDirectory, dir];
        BOOL isDir = NO;
        BOOL existed = [fm fileExistsAtPath:subDir isDirectory:&isDir];
        if (!(isDir == YES && existed == YES)) {
            [fm createDirectoryAtPath:subDir withIntermediateDirectories:YES attributes:nil error:nil];
        }
        
        writableDBPath = [[documentsDirectory stringByAppendingPathComponent:dir] stringByAppendingPathComponent:name];
        
    } else
        writableDBPath = [documentsDirectory stringByAppendingPathComponent:name];
    
    //    NSLog(@"writableDBPath:%@",writableDBPath);
    
    success = [fm fileExistsAtPath:writableDBPath];
    
    if (ovrid && success) {
        
        NSError *error;
        if ([fm removeItemAtPath:writableDBPath error:&error])
            NSLog(@"Ovrride database file success");
        
    }
    
    if (ovrid || !success) {
        
        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:name];
        success = [fm copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
        if (!success) {
            NSLog(@"copy database file error:%@",[error localizedDescription]);
            success = NO;
        }
        
    }
    
    
    if (success) {
        
        return writableDBPath;
        
    }
    
    return nil;
    
}



@end
