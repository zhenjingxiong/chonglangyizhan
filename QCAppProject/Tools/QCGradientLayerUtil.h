//
//  QCGradientLayerUtil.h
//  QCAppProject
//
//  Created by yjl on 2021/8/16.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QCGradientLayerUtil : NSObject

+ (void)configLayerGradientColor:(UIView *)view originColor:(UIColor *)originColor targetColor:(UIColor *)targetColor;


@end

NS_ASSUME_NONNULL_END
