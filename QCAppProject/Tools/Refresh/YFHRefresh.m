//
//  YFHRefresh.m
//  yfhmall
//
//  Created by mac1 on 2017/7/27.
//  Copyright © 2017年 dwq. All rights reserved.
//

#import "YFHRefresh.h"
#import <MJRefresh/MJRefresh.h>

@interface YFHRefresh () {
    NSArray *idleImages;
    NSArray *pullingImages;
    NSArray *refreshingImages;
}

//下拉时候触发的block
@property (nonatomic, copy) void(^DropDownRefreshBlock)(void);
//上拉时候触发的block
@property (nonatomic, copy) void(^UpDropRefreshBlock)(void);
//接收传递过来的参数
@property (nonatomic, strong) UIScrollView *parameterTableView;


@end

@implementation YFHRefresh

- (instancetype)init {
    if (self = [super init]) {
        //此gif为逐帧动画由多张图片组成
        //闲置状态下的gif(就是拖动的时候变化的gif)
        idleImages = [[NSArray alloc] initWithObjects:fImageNamed(@"Image"), fImageNamed(@"Image1"), fImageNamed(@"Image2"), fImageNamed(@"Image3"), fImageNamed(@"Image4"), fImageNamed(@"Image5"), nil];
        //已经到达偏移量的gif(就是已经到达偏移量的时候的gif)
        pullingImages = [[NSArray alloc] initWithObjects:fImageNamed(@"Image"), fImageNamed(@"Image1"), fImageNamed(@"Image2"), fImageNamed(@"Image3"), fImageNamed(@"Image4"), fImageNamed(@"Image5"), nil];
        //正在刷新的时候的gif
        refreshingImages = [[NSArray alloc] initWithObjects:fImageNamed(@"Image"), fImageNamed(@"Image1"), fImageNamed(@"Image2"), fImageNamed(@"Image3"), fImageNamed(@"Image4"), fImageNamed(@"Image5"), nil];
    }
    return self;
}


//正常模式下拉上拉刷新(firstRefresh第一次进入的时候是否要刷新,这个值只对下拉刷新有影响)(refreshType设置为只支持上拉或者下拉的时候,将另外一个block置为nil)
- (void)normalModelRefresh:(UIScrollView *)tableView refreshType:(RefreshType)refreshType firstRefresh:(BOOL)firstRefresh timeLabHidden:(BOOL)timeLabHidden stateLabHidden:(BOOL)stateLabHidden dropDownBlock:(void(^)(void))dropDownBlock upDropBlock:(void(^)(void))upDropBlock {
    _parameterTableView = tableView;
    if (refreshType == RefreshTypeDropDown) {
        //只支持下拉
        //将block传入
        self.DropDownRefreshBlock = dropDownBlock;
        //初始化
        MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(dropDownBlockAction)];
        //是否隐藏上次更新的时间
        header.lastUpdatedTimeLabel.hidden = timeLabHidden;
        //是否隐藏刷新状态label
        header.stateLabel.hidden = stateLabHidden;
        //隐藏时间
        header.lastUpdatedTimeLabel.hidden = YES;
        //设置文字
        [header setTitle:@"下拉可以刷新..." forState:MJRefreshStateIdle];
        [header setTitle:@"松开立即刷新..." forState:MJRefreshStatePulling];
        [header setTitle:@"正在刷新数据中..." forState:MJRefreshStateRefreshing];
        //tableView.mj_header接收header
        tableView.mj_header = header;
        //首次进来是否需要刷新
        if (firstRefresh) {
            [tableView.mj_header beginRefreshing];
        }
        //透明度渐变
        tableView.mj_header.automaticallyChangeAlpha = YES;
    }else if (refreshType == RefreshTypeUpDrop) {
        //只支持上拉
        //传入block
        self.UpDropRefreshBlock = upDropBlock;
        //初始化并指定方法
        tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(upDropBlockAction)];
        //指定数据加载完毕的文字
        [(MJRefreshAutoNormalFooter *)tableView.mj_footer setTitle:@"我是有底线的哦!" forState:MJRefreshStateNoMoreData];
        [(MJRefreshAutoNormalFooter *)tableView.mj_footer setTitle:@"正在加载数据中..." forState:MJRefreshStateRefreshing];
        [(MJRefreshAutoNormalFooter *)tableView.mj_footer setTitle:@"" forState:MJRefreshStateIdle];
        [(MJRefreshAutoNormalFooter *)tableView.mj_footer setTitle:@"" forState:MJRefreshStatePulling];
        
    }else if (refreshType == RefreshTypeDouble) {
        //上拉和下拉都持支持
        //下拉
        //将block传入
        self.DropDownRefreshBlock = dropDownBlock;
        //初始化
        MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(dropDownBlockAction)];
        //是否隐藏上次更新的时间
        header.lastUpdatedTimeLabel.hidden = timeLabHidden;
        //是否隐藏刷新状态label
        header.stateLabel.hidden = stateLabHidden;
        //隐藏时间
        header.lastUpdatedTimeLabel.hidden = YES;
        //设置文字
        [header setTitle:@"下拉可以刷新..." forState:MJRefreshStateIdle];
        [header setTitle:@"松开立即刷新..." forState:MJRefreshStatePulling];
        [header setTitle:@"正在刷新数据中..." forState:MJRefreshStateRefreshing];
        //tableView.mj_header接收header
        tableView.mj_header = header;
        //首次进来是否需要刷新
        if (firstRefresh) {
            [tableView.mj_header beginRefreshing];
        }
        //透明度渐变
        tableView.mj_header.automaticallyChangeAlpha = YES;
        //上拉
        //将block传入
        self.UpDropRefreshBlock = upDropBlock;   
        //初始化并指定方法
//        tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(upDropBlockAction)];//自动弹回
        tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(upDropBlockAction)];//留有刷新的位置
        //指定数据加载完毕的文字
        [(MJRefreshAutoNormalFooter *)tableView.mj_footer setTitle:@"我是有底线的哦!" forState:MJRefreshStateNoMoreData];
        [(MJRefreshAutoNormalFooter *)tableView.mj_footer setTitle:@"正在加载数据中..." forState:MJRefreshStateRefreshing];
        [(MJRefreshAutoNormalFooter *)tableView.mj_footer setTitle:@"" forState:MJRefreshStateIdle];
        [(MJRefreshAutoNormalFooter *)tableView.mj_footer setTitle:@"" forState:MJRefreshStatePulling];
        
    }
}

//gifRefresh
- (void)gifModelRefresh:(UIScrollView *)tableView refreshType:(RefreshType)refreshType firstRefresh:(BOOL)firstRefresh timeLabHidden:(BOOL)timeLabHidden stateLabHidden:(BOOL)stateLabHidden dropDownBlock:(void(^)(void))dropDownBlock upDropBlock:(void(^)(void))upDropBlock {
    _parameterTableView = tableView;
    if (refreshType == RefreshTypeDropDown) {
        //只支持下拉
        self.DropDownRefreshBlock = dropDownBlock;
        MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingTarget:self refreshingAction:@selector(dropDownBlockAction)];
        [header setImages:idleImages forState:MJRefreshStateIdle];
        [header setImages:pullingImages forState:MJRefreshStatePulling];
        [header setImages:refreshingImages forState:MJRefreshStateRefreshing];
        header.lastUpdatedTimeLabel.hidden = YES;
        header.lastUpdatedTimeLabel.hidden = timeLabHidden;
        header.stateLabel.hidden = stateLabHidden;
        tableView.mj_header = header;
        if (firstRefresh) {
            [tableView.mj_header beginRefreshing];
        }
        
        //设置文字
        [header setTitle:@"下拉可以刷新..." forState:MJRefreshStateIdle];
        [header setTitle:@"松开立即刷新..." forState:MJRefreshStatePulling];
        [header setTitle:@"正在刷新数据中..." forState:MJRefreshStateRefreshing];
        
    }else if (refreshType == RefreshTypeUpDrop) {
        //只支持上拉
        self.UpDropRefreshBlock = upDropBlock;
        //初始化并指定方法
        tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(upDropBlockAction)];
        //指定数据加载完毕的文字
        [(MJRefreshAutoNormalFooter *)tableView.mj_footer setTitle:@"我是有底线的哦!" forState:MJRefreshStateNoMoreData];
        [(MJRefreshAutoNormalFooter *)tableView.mj_footer setTitle:@"正在加载数据中..." forState:MJRefreshStateRefreshing];
        [(MJRefreshAutoNormalFooter *)tableView.mj_footer setTitle:@"" forState:MJRefreshStateIdle];
        [(MJRefreshAutoNormalFooter *)tableView.mj_footer setTitle:@"" forState:MJRefreshStatePulling];
    }else if (refreshType == RefreshTypeDouble) {
        //支持上拉和下拉加载
        self.DropDownRefreshBlock = dropDownBlock;
        MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingTarget:self refreshingAction:@selector(dropDownBlockAction)];
        [header setImages:idleImages forState:MJRefreshStateIdle];
        [header setImages:pullingImages forState:MJRefreshStatePulling];
        [header setImages:refreshingImages forState:MJRefreshStateRefreshing];
        header.lastUpdatedTimeLabel.hidden = YES;
        header.lastUpdatedTimeLabel.hidden = timeLabHidden;
        header.stateLabel.hidden = stateLabHidden;
        
        //设置文字
        [header setTitle:@"下拉可以刷新..." forState:MJRefreshStateIdle];
        [header setTitle:@"松开立即刷新..." forState:MJRefreshStatePulling];
        [header setTitle:@"正在刷新数据中..." forState:MJRefreshStateRefreshing];
        
        tableView.mj_header = header;
        if (firstRefresh) {
            [tableView.mj_header beginRefreshing];
        }
        self.UpDropRefreshBlock = upDropBlock;
        //初始化并指定方法
        tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(upDropBlockAction)];
        //指定数据加载完毕的文字
        [(MJRefreshAutoNormalFooter *)tableView.mj_footer setTitle:@"我是有底线的哦!" forState:MJRefreshStateNoMoreData];
        [(MJRefreshAutoNormalFooter *)tableView.mj_footer setTitle:@"正在加载数据中..." forState:MJRefreshStateRefreshing];
        [(MJRefreshAutoNormalFooter *)tableView.mj_footer setTitle:@"" forState:MJRefreshStateIdle];
        [(MJRefreshAutoNormalFooter *)tableView.mj_footer setTitle:@"" forState:MJRefreshStatePulling];
    }
}

//下拉时候触发的block
- (void)dropDownBlockAction {
    if (_DropDownRefreshBlock) {
        _DropDownRefreshBlock();
        [_parameterTableView.mj_footer resetNoMoreData];
    }
}

//上拉时候触发的block
- (void)upDropBlockAction {
    if (_UpDropRefreshBlock) {
        _UpDropRefreshBlock();
    }
}


@end
