//
//  FDDKeyChain.m
//  AppIOSProject
//
//  Created by jiale yang on 2021/4/19.
//  Copyright © 2021 FaDaDaIT. All rights reserved.
//

#import "FDDKeyChain.h"
#import "SAMKeychain.h"

static NSString *const kPDDKeyChainKey = @"com.fdd.keychainKey";

@implementation FDDKeyChain

// 传2个参数，service：服务名，key：键名
// 一般情况1个服务下，会包含多个内容

// 获取内容
+ (NSString *) getContentFromKeyChain:(NSString *) key {
    return [SAMKeychain passwordForService:kPDDKeyChainKey account:key];
}

//向keyChain中保存内容
+ (BOOL) saveContentToKeyChain: (NSString *) content
                                 forKey: (NSString *) key {
    return [SAMKeychain setPassword:content forService:kPDDKeyChainKey account:key];
}

//清除keyChain中的数据
+ (BOOL) removeContentFromKeyChain: (NSString *) key {
    return [SAMKeychain deletePasswordForService:kPDDKeyChainKey account:key];
}
@end
