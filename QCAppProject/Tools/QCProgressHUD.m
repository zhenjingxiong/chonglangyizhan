//
//  QCProgressHUD.m
//  QCAppProject
//
//  Created by yjl on 2021/8/16.
//

#import "QCProgressHUD.h"
#import "MBProgressHUD.h"

@implementation QCProgressHUD

+ (void)showHUDWithMessage:(NSString*)message autoHide:(BOOL)needAutoHide {
    [self hideHUD];
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:window animated:YES];
    hud.color = kHexColor(@"666666");
    hud.mode = MBProgressHUDModeText;
    hud.detailsLabelText = message;
    hud.layer.zPosition = 9999;
    hud.removeFromSuperViewOnHide = YES;

    if (needAutoHide)
    [hud hide:YES afterDelay:1.3];
}

+ (void)changeHUDMessage:(NSString*)message {

    [self hideHUD];
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:window animated:YES];
    hud.color = kHexColor(@"666666");
    hud.mode = MBProgressHUDModeText;
    hud.detailsLabelText = message;
    hud.layer.zPosition = 9999;
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:1.3];
}

+ (void)hideHUD {

    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    [MBProgressHUD hideAllHUDsForView:window animated:YES];
}

+ (void)showHUDWithWaitingMessage:(NSString *)message {

    [self hideHUD];
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:window animated:YES];
    hud.color = kHexColor(@"666666");
    hud.userInteractionEnabled = NO;
    hud.mode = MBProgressHUDModeIndeterminate;
    if (![self isBlankString:message]) {
        hud.detailsLabelText = message;
    }
    hud.layer.zPosition = 9999;
    hud.removeFromSuperViewOnHide = YES;

}

+ (MBProgressHUD *)showHUDWithProgress:(float)progress Mode:(MBProgressHUDMode)mode message:(NSString *)message
{
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:window animated:YES];
    hud.color = kHexColor(@"666666");
    if (mode == MBProgressHUDModeDeterminate || mode == MBProgressHUDModeDeterminateHorizontalBar) {
        hud.mode = mode;
    } else {
        hud.mode = MBProgressHUDModeAnnularDeterminate;
    }
    if (![self isBlankString:message]) {
        hud.detailsLabelText = message;
    }
    hud.progress = progress;
    hud.layer.zPosition = 9999;
    hud.removeFromSuperViewOnHide = YES;
    return hud;
}

+ (BOOL)isBlankString:(NSString *)string
{
    if (string == nil || string == NULL) {
        return YES;
    }
    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    return NO;
}



/**
 @brief 显示信息
 @param text    文字
 @param icon    图片
 */
+ (void)show:(NSString *)text icon:(NSString *)icon view:(UIView *)view
{
//    if (view == nil) view = [UIApplication sharedApplication].keyWindow;
//    // 快速显示一个提示信息
//    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
//    hud.color = kHexColor(@"666666");
//    hud.detailsLabelText = text;
//    hud.detailsLabelFont = [UIFont systemFontOfSize:14];
//    // 设置图片
//    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:icon]];
//    // 再设置模式
//    hud.mode = MBProgressHUDModeCustomView;
//    // 隐藏时候从父控件中移除
//    hud.removeFromSuperViewOnHide = YES;
//    // 1秒之后再消失
//    [hud hide:YES afterDelay:1.5];
}


/**
 @brief 显示错误信息
 @param error    错误文字
 */
+ (void)showError:(NSString *)error toView:(UIView *)view {
    
    [self show:error icon:@"login_pop_no" view:view];
}



/**
 @brief 显示成功信息
 @param success    成功文字
 */

+ (void)showSuccess:(NSString *)success toView:(UIView *)view {
    
    [self show:success icon:@"login_pop_yes" view:view];
}





@end
