//
//  QCProgressHUD.h
//  QCAppProject
//
//  Created by yjl on 2021/8/16.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QCProgressHUD : NSObject

+ (void)showHUDWithMessage:(NSString*)message autoHide:(BOOL)needAutoHide;

+ (void)changeHUDMessage:(NSString*)message;

+ (void)hideHUD;

+ (void)showHUDWithWaitingMessage:(NSString *)message;

//+ (MBProgressHUD *)showHUDWithProgress:(float)progress Mode:(MBProgressHUDMode)mode message:(NSString *)message;

/**
 @brief 显示错误信息
 @param error    错误信息
 */

+ (void)showError:(NSString *)error toView:(UIView *)view;

/**
@brief 显示成功信息
 @param success    成功文字
*/

+ (void)showSuccess:(NSString *)success toView:(UIView *)view;



@end

NS_ASSUME_NONNULL_END
