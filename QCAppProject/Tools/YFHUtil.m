//
//  YFHUtil.m
//  yfhmall
//
//  Created by mac1 on 2017/7/21.
//  Copyright © 2017年 dwq. All rights reserved.
//

#import "YFHUtil.h"

@implementation YFHUtil

#pragma mark -
#pragma mark string is blank
+ (BOOL)isBlankString:(NSString *)string
{
    if (string == nil || string == NULL) {
        return YES;
    }
    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([string isEqualToString:@"(null)"] || [string isEqualToString:@"<null>"] ||[string isEqualToString:@"null"] ) {
        return YES;
    }
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    return NO;
}


@end
