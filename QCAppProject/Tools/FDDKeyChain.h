//
//  FDDKeyChain.h
//  AppIOSProject
//
//  Created by jiale yang on 2021/4/19.
//  Copyright © 2021 FaDaDaIT. All rights reserved.
//

#import <Foundation/Foundation.h>


NS_ASSUME_NONNULL_BEGIN

@interface FDDKeyChain : NSObject

// 获取内容
+ (NSString *) getContentFromKeyChain:(NSString *) key;

//向keyChain中保存内容
+ (BOOL) saveContentToKeyChain: (NSString *) content
                        forKey: (NSString *) key;

//清除keyChain中的数据
+ (BOOL) removeContentFromKeyChain: (NSString *) key;

@end

NS_ASSUME_NONNULL_END
