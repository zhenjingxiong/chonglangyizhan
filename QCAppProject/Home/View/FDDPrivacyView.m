//
//  FDDPrivacyView.m
//  NewFDD
//
//  Created by IOSdev on 2020/11/10.
//  Copyright © 2020 wenwen. All rights reserved.
//

#import "FDDPrivacyView.h"

@interface FDDPrivacyView ()<UITextViewDelegate>




@end




@implementation FDDPrivacyView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        UIView *bgView = [[UIView alloc]init];
        bgView.backgroundColor = [UIColor blackColor];
        bgView.alpha = 0.5;
        [self addSubview:bgView];
        
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.top.left.right.bottom.equalTo(@0);
        }];
        
        UIView  *whiteView = [[UIView alloc]init];
        whiteView.userInteractionEnabled = YES;
        whiteView.layer.borderWidth = 0.6;
        whiteView.layer.borderColor = kHexColor(@"#DEE0E6").CGColor;
        whiteView.layer.backgroundColor = [UIColor whiteColor].CGColor;
        whiteView.layer.cornerRadius = 12;

        [self addSubview:whiteView];
        
        [whiteView mas_makeConstraints:^(MASConstraintMaker *make) {
                   
                   make.centerY.equalTo(self.mas_centerY);
                   make.left.equalTo(@(AD_W(44)));
                   make.right.equalTo(@(-AD_W(44)));
                   make.height.equalTo(@(AD_H(334)));
        }];
        
        UILabel *tipTitleLable = [[UILabel alloc]init];
        tipTitleLable.textAlignment = NSTextAlignmentCenter;
        tipTitleLable.text = @"协议与政策";
        tipTitleLable.font = FONT_PM(18);
        [whiteView addSubview:tipTitleLable];
        
        [tipTitleLable mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.centerX.equalTo(whiteView.mas_centerX);
            make.top.equalTo(@(AD_H(30)));
            make.width.equalTo(@200);
            make.height.equalTo(@(AD_H(20)));
        }];
        
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]initWithString:@"感谢您信任并使用奇成科技的产品。请您务必审慎阅读、充分理解用户协议和隐私政策各条款，包括但不限于服务使用须知、用户行为规范以及为了向您提供服务而收集、使用、存储您个人信息的情况等。您可阅读《用户协议》和《隐私政策》了解详细信息。如您同意，请点击“同意”开始接受我们的服务。"];
        [attributedString addAttribute:NSLinkAttributeName
                                 value:@"useagreement://"
                                 range:[[attributedString string] rangeOfString:@"《用户协议》"]];
        
        [attributedString addAttribute:NSLinkAttributeName
                                 value:@"privacy://"
                                 range:[[attributedString string] rangeOfString:@"《隐私政策》"]];
        
        
        //具体内容
        UITextView *text = [[UITextView  alloc]init];
         text.attributedText = attributedString;
         text.editable = NO;
         text.scrollEnabled = YES;
         text.delegate = self;
         [whiteView addSubview:text];
        
        

        [attributedString addAttribute:NSFontAttributeName value:FONT_PR(14) range:NSMakeRange(0, attributedString.length)];
        text.attributedText = attributedString;
        text.linkTextAttributes = @{NSForegroundColorAttributeName: QCOrange,
                                    NSUnderlineColorAttributeName: [UIColor clearColor],
                                    NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
        
        

        
        
        _notAgreeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [whiteView addSubview:_notAgreeBtn];
        _notAgreeBtn.layer.cornerRadius = 8;
        _notAgreeBtn.layer.borderColor = kHexColor(@"#E7E8EA").CGColor;
        _notAgreeBtn.layer.borderWidth = 1;

        [_notAgreeBtn setTitle:@"不同意" forState:UIControlStateNormal];
        _notAgreeBtn.backgroundColor = [UIColor whiteColor];
        
        [_notAgreeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_notAgreeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.left.equalTo(@(AD_W(16)));
            make.right.equalTo(whiteView.mas_centerX).offset(-5);
            make.bottom.equalTo(whiteView.mas_bottom).offset(-12);
            make.height.equalTo(@(AD_H(44)));
        }];
        
        _agreeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [whiteView addSubview:_agreeBtn];
        [_agreeBtn setTitle:@"我同意" forState:UIControlStateNormal];
        _agreeBtn.backgroundColor = QCOrange;
        
        [_agreeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _agreeBtn.layer.cornerRadius = 8;
        
        [_agreeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.left.equalTo(whiteView.mas_centerX).offset(5);
            make.right.equalTo(@(-AD_W(16)));
            make.bottom.equalTo(whiteView.mas_bottom).offset(-12);
            make.height.equalTo(@(AD_H(44)));
        }];
        
        
        [text mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.left.equalTo(@(AD_W(24)));
            make.right.equalTo(@(-AD_W(24)));
            make.top.equalTo(tipTitleLable.mas_bottom).offset(AD_H(16));
            make.bottom.equalTo(self.notAgreeBtn.mas_top).offset(-AD_H(16));

        }];
        
        
    }
    return self;
}


- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange {
    
    if (self.privacyBtnBlock) {
    
        self.privacyBtnBlock([URL scheme]);
    }
    return YES;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
