//
//  FDDPrivacyView.h
//  NewFDD
//
//  Created by IOSdev on 2020/11/10.
//  Copyright © 2020 wenwen. All rights reserved.
//

/**
  协议与政策按钮
 */


#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^PrivacyBtnBlock)(NSString  *URL);

@interface FDDPrivacyView : UIView


//同意按钮
@property (strong, nonatomic) UIButton *agreeBtn;

//不同意按钮
@property (strong, nonatomic) UIButton *notAgreeBtn;

//点击跳转用户协议和隐私政策
@property (copy,nonatomic)PrivacyBtnBlock  privacyBtnBlock;





@end

NS_ASSUME_NONNULL_END
