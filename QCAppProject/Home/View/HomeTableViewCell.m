//
//  HomeTableViewCell.m
//  QCAppProject
//
//  Created by yjl on 2021/8/17.
//

#import "HomeTableViewCell.h"
#import "QCInformationModel.h"

@interface HomeTableViewCell ()

@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UILabel *subtitleLbl;
@property (nonatomic, strong) UILabel *timeLbl;
@property (nonatomic, strong) UIView *horLine;

@end

@implementation HomeTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self == [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initConstraints];
    }
    return self;
}

- (void)configCellData:(id)sender {
    if (![sender isKindOfClass:[QCInformationModel class]]) {
        return;
    }
    QCInformationModel *model = sender;
    self.titleLbl.text = model.title;
    self.subtitleLbl.text = model.remark;
    self.timeLbl.text = model.createDateStr;
}

- (void)initConstraints {
    UIView *superView = self.contentView;
    [superView addSubview:self.containerView];
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left);
        make.right.equalTo(superView.mas_right);
        make.top.equalTo(superView.mas_top);
        make.bottom.equalTo(superView.mas_bottom);
    }];
    
    [self.containerView addSubview:self.titleLbl];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@16);
        make.top.equalTo(@16);
        make.width.equalTo(@(SCREEN_WIDTH - 32));
    }];
    
    [self.containerView addSubview:self.subtitleLbl];
    [self.subtitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLbl.mas_left);
        make.top.equalTo(self.titleLbl.mas_bottom).offset(8);
        make.width.equalTo(@(SCREEN_WIDTH - 32));
    }];
    
    [self.containerView addSubview:self.timeLbl];
    [self.timeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@-16);
        make.top.equalTo(self.subtitleLbl.mas_bottom).offset(10);
    }];
    
    [self.containerView addSubview:self.horLine];
    [self.horLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(@0);
        make.top.equalTo(self.timeLbl.mas_bottom).offset(8);
        make.height.equalTo(@(0.6));
    }];
    
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.horLine.mas_bottom);
    }];
}

- (UIView *)containerView {
    if (!_containerView) {
        _containerView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return _containerView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _titleLbl.font = FONT_PR(15);
        _titleLbl.textColor = QCBlack;
        _titleLbl.text = @"物联网的核心是什么?-创造高价值";
        _titleLbl.numberOfLines = 0;
        [_titleLbl sizeToFit];//自适应，在这可写可不写，一般在frame布局时，设置width没有设置高度，调用此方法可自动适应高度
    }
    return _titleLbl;
}

- (UILabel *)subtitleLbl {
    if (!_subtitleLbl) {
        _subtitleLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _subtitleLbl.font = FONT_PR(13);
        _subtitleLbl.textColor = QCSubBlack;
        _subtitleLbl.text = @"在对抗新冠疫情的战斗中，人工智能技术在医疗行2业的应用 发挥了举足轻重的作用。";
        _subtitleLbl.numberOfLines = 0;
        [_subtitleLbl sizeToFit];
    }
    return _subtitleLbl;
}

- (UILabel *)timeLbl {
    if (!_timeLbl) {
        _timeLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _timeLbl.font = FONT_PR(11);
        _timeLbl.textColor = QCDesBlack;
        _timeLbl.text = @"2020年1月8日";
        [_timeLbl sizeToFit];
    }
    return _timeLbl;
}

- (UIView *)horLine {
    if (!_horLine) {
        _horLine = [[UIView alloc] initWithFrame:CGRectZero];
        _horLine.backgroundColor = QCLineColor;
    }
    return _horLine;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
