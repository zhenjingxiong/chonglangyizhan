//
//  HomeTopView.h
//  QCAppProject
//
//  Created by yjl on 2021/8/17.
//

#import <UIKit/UIKit.h>

typedef void(^ItemClickedBlock)(NSInteger index);

NS_ASSUME_NONNULL_BEGIN

@interface HomeTopView : UIView

@property (nonatomic, copy) ItemClickedBlock itemClickedBlock;

@end

NS_ASSUME_NONNULL_END
