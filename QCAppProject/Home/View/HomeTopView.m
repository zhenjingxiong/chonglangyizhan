//
//  HomeTopView.m
//  QCAppProject
//
//  Created by yjl on 2021/8/17.
//

#import "HomeTopView.h"
#import "UIButton+MutableButton.h"

@interface HomeTopView ()

@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIView *noticeView;
@property (nonatomic, strong) UIView *itemView;


@end

@implementation HomeTopView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self == [super initWithFrame:frame]) {
        [self initConstraints];
    }
    return self;
}

- (void)viewToClicked:(UITapGestureRecognizer *)tap{
    if (self.itemClickedBlock) {
        self.itemClickedBlock(tap.view.tag - 1000);
    }
}

- (void)initConstraints {
    [self addSubview:self.bgView];
    
    UIImageView *bgimageView = [[UIImageView alloc] initWithFrame:self.bgView.frame];
    bgimageView.image = fImageNamed(@"home_bg");
    [self.bgView addSubview:bgimageView];
    [self.bgView addSubview:self.noticeView];
    [self.bgView addSubview:self.itemView];
    
    NSArray *arr = @[@"查询服务",@"咨询服务",@"物联资讯",@"在线客服"];
    NSArray *arrImages = @[@"home_query",@"home_consult",@"home_infomation",@"home_cooperation"];
    for (NSInteger i = 0; i < arr.count; i++) {
        UIView *btnView = [[UIView alloc] initWithFrame:CGRectMake(i*SCREEN_WIDTH/arr.count,(self.itemView.height-56)/2, SCREEN_WIDTH/arr.count, 56)];
        [btnView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewToClicked:)]];
        btnView.tag = 1000 + i;
        UIImage *image = fImageNamed(arrImages[i]);
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        imageView.frame = CGRectMake((btnView.width - image.size.width)/2, 0, image.size.width, image.size.height);
        [btnView addSubview:imageView];
        
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, imageView.bottom + 10, btnView.width, 14)];
        lbl.font = FONT_PR(15);
        lbl.textColor = QCWhite;
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.text = arr[i] ;
        [btnView addSubview:lbl];
        [self.itemView addSubview:btnView];
    }
}


//- (void)cbtn
//-(void)setButtonContentCenter:(UIButton *) btn {
//    CGSize imgViewSize,titleSize,btnSize;
//    UIEdgeInsets imageViewEdge,titleEdge;
//    CGFloat heightSpace = 10.0f;
//    //设置按钮内边距
//    imgViewSize = btn.imageView.bounds.size;
//    titleSize = btn.titleLabel.bounds.size;
//    btnSize = btn.bounds.size;
//    imageViewEdge = UIEdgeInsetsMake(heightSpace,0.0, btnSize.height -imgViewSize.height - heightSpace, - titleSize.width);
//    [btn setImageEdgeInsets:imageViewEdge];
//
//    titleEdge = UIEdgeInsetsMake(imgViewSize.height +heightSpace, - imgViewSize.width, 0.0, 0.0);
//    [btn setTitleEdgeInsets:titleEdge];
//}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 175)];
    }
    return _bgView;
}

- (UIView *)noticeView {
    if (!_noticeView) {
        _noticeView = [[UIView alloc] initWithFrame:CGRectMake(16, kStatusBarHeight+5, SCREEN_WIDTH - 32, 24)];
        _noticeView.backgroundColor = QCWhite;
        _noticeView.layer.cornerRadius = 12.0f;
        UIImageView *noticeImage = [[UIImageView alloc] initWithImage:fImageNamed(@"home_notice")];
        noticeImage.frame =CGRectMake(10, 5, 12, 14);
        [_noticeView addSubview:noticeImage];
        UILabel *titleLbl = [[UILabel alloc] initWithFrame:self.noticeView.bounds];
        titleLbl.font = FONT_PR(11);
        titleLbl.text = @"每天一语：4G改变生活，5G改变社会每天一语";
        titleLbl.textColor = kHexColor(@"948B8B");
        titleLbl.numberOfLines = 1;
        titleLbl.left = noticeImage.right + 8;
        titleLbl.width = _noticeView.width - 40;
        [_noticeView addSubview:titleLbl];
    }
    return _noticeView;
}

- (UIView *)itemView {
    if (!_itemView) {
        _itemView = [[UIView alloc] initWithFrame:CGRectMake(0, self.noticeView.bottom + 10, SCREEN_WIDTH, self.bgView.height - self.noticeView.bottom-10)];
    }
    return _itemView;
}

@end
