//
//  HomeTableHeadView.m
//  QCAppProject
//
//  Created by yjl on 2021/8/17.
//

#import "HomeTableHeadView.h"

@interface HomeTableHeadView ()

@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UILabel *subtitleLbl;
@property (nonatomic, strong) UIView *horLine;

@end

@implementation HomeTableHeadView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self == [super initWithFrame:frame]) {
        [self addSubview:self.titleLbl];
        [self.titleLbl setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
        [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@16);
            make.centerY.equalTo(self.mas_centerY);
        }];
        
        [self.subtitleLbl setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
        [self addSubview:self.subtitleLbl];
        [self.subtitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.titleLbl.mas_right).offset(8);
            make.centerY.equalTo(self.mas_centerY);
            make.right.equalTo(@-8);
        }];
        [self addSubview:self.horLine];
        [self.horLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.and.right.equalTo(@0);
            make.bottom.equalTo(self.mas_bottom);
            make.height.equalTo(@(0.6));
        }];
    }
    return self;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _titleLbl.font = FONT_PR(13);
        _titleLbl.textColor = QCBlack;
        _titleLbl.text = @"为您精选";
        [_titleLbl sizeToFit];
    }
    return _titleLbl;
}

- (UILabel *)subtitleLbl {
    if (!_subtitleLbl) {
        _subtitleLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _subtitleLbl.font = FONT_PR(11);
        _subtitleLbl.textColor = QCTipBlack;
        _subtitleLbl.text = @"精准推送 想你所想";
        [_subtitleLbl sizeToFit];
    }
    return _subtitleLbl;
}

- (UIView *)horLine {
    if (!_horLine) {
        _horLine = [[UIView alloc] initWithFrame:CGRectZero];
        _horLine.backgroundColor = QCLineColor;
    }
    return _horLine;
}


@end
