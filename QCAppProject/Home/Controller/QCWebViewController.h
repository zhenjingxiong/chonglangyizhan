//
//  QCWebViewController.h
//  QCAppProject
//
//  Created by yjl on 2021/8/20.
//

#import "QCBaseViewController.h"


typedef NS_ENUM(NSUInteger, QCQueryLinkType) {
    QCQueryLinkDefault = 0,
    QCQueryLinkFefu = 1,
};

NS_ASSUME_NONNULL_BEGIN

@interface QCWebViewController : QCBaseViewController


@property(nonatomic,strong) NSString *url;

@property(nonatomic,assign) QCQueryLinkType queryLinkType;

@end

NS_ASSUME_NONNULL_END
