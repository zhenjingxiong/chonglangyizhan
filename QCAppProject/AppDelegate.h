//
//  AppDelegate.h
//  QCAppProject
//
//  Created by yjl on 2021/8/13.
//

#import <UIKit/UIKit.h>
#import "QCTabBarController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;

@property (nonatomic, strong) QCTabBarController *tabBarController;

@end

