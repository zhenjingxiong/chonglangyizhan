//
//  QCNetworkManager.h
//  QCAppProject
//
//  Created by yjl on 2021/8/19.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"


@interface QCNetworkManager : NSObject

+ (NSURLSessionDataTask *)requestWithURL:(NSString *)url
            parameters:(NSDictionary *)parameters
              Progress:(void (^)(CGFloat progress))Progressblock
               success:(void(^)(id responseObject))success
               failure:(void(^)(NSError *error))failure;

+ (NSURLSessionDataTask *)requestWithURL:(NSString *)url
            parameters:(NSDictionary *)parameters
      constructingBody:(void(^)(id<AFMultipartFormData> formData))constructingBody
              Progress:(void (^)(CGFloat progress))Progressblock
               success:(void(^)(id responseObject))success
               failure:(void(^)(NSError *error))failure;

/**
 get
 */
+ (NSURLSessionDataTask *)getRequestWithURL:(NSString *)url
            parameters:(NSDictionary *)parameters
              Progress:(void (^)(CGFloat progress))Progressblock
               success:(void(^)(id responseObject))success
               failure:(void(^)(NSError *error))failure;


+ (NSURLSessionDataTask *)auditRequestWithURL:(NSString *)url
            parameters:(NSDictionary *)parameters
              Progress:(void (^)(CGFloat progress))Progressblock
               success:(void(^)(id responseObject))success
               failure:(void(^)(NSError *error))failure;

@end

