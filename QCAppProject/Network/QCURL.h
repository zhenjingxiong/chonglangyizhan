//
//  QCURL.h
//  QCAppProject
//
//  Created by yjl on 2021/8/19.
//

#import <Foundation/Foundation.h>

//上线时需要设置
#define HOST_URL_OPTION      2
#if(HOST_URL_OPTION == 1) //正式环境
   #define BASE_IP @"https://iot.qichenglink.com" //线上
#elif(HOST_URL_OPTION == 2) //内部测试服务器环境
   #define BASE_IP  @"https://iot.qichenglink.com"
#elif(HOST_URL_OPTION == 3) //外网测试服务器环境
   #define BASE_IP  @"https://iot.qichenglink.com"
#endif

NS_ASSUME_NONNULL_BEGIN

@interface QCURL : NSObject

//查询服务
@property (nonatomic, copy) NSString *queryKefuUrl;
//在线客服
@property (nonatomic, copy) NSString *queryLinkUrl;
//公司信息
@property (nonatomic, copy) NSString *companyInfoUrl;

//公告通知
@property (nonatomic, copy) NSString *noticeUrl;

//列表wstatus = 1待处理 2已处理
//咨询查询 == 可根据id，workerNo来查详情，更具userPhone查用户的--
// -- https://iot.qichenglink.com/appworkerorder/getAppWorkerOrder.do?id=&workerNo=&userPhone=
@property (nonatomic, copy) NSString *workerOrderUrl;

//咨询保存 -- https://iot.qichenglink.com/appworkerorder/addSave.do?userPhone=13000000000&content=%E6%B5%8B%E8%AF%95
@property (nonatomic, copy) NSString *workerOrderSaveUrl;

//短信 https://iot.qichenglink.com/appuser/sendSms.do?userphone=
@property (nonatomic, copy) NSString *smsCodeUrl;

//用户注册 https://iot.qichenglink.com/appuser/addSave.do?userphone=&ucode=
@property (nonatomic, copy) NSString *loginUrl;

- (instancetype _Nullable )initBaseURL;

@end

/*
 
 所有接口
 app_result_key=0;接口成功
 system_result_message_key错误信息
 data 需要的数据（部分接口返回的是数组）
 
 */

NS_ASSUME_NONNULL_END
