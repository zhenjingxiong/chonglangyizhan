//
//  QCURL.m
//  QCAppProject
//
//  Created by yjl on 2021/8/19.
//

#import "QCURL.h"

@implementation QCURL

- (instancetype)initBaseURL {
    if (self == [super init]) {
        _queryLinkUrl = [NSString stringWithFormat:@"http://iot.qichenglink.com/appquerylink/getAppQueryLink.do"];
        _queryKefuUrl = [NSString stringWithFormat:@"http://iot.qichenglink.com/appquerylink/getAppKefuLink.do"];
        _companyInfoUrl = [NSString stringWithFormat:@"%@/appcompany/getAppCompany.do",BASE_IP];
        _noticeUrl = [NSString stringWithFormat:@"%@/appnotice/getAppNotice.do",BASE_IP];
        _workerOrderUrl = [NSString stringWithFormat:@"%@/appworkerorder/getAppWorkerOrder.do",BASE_IP];
        _workerOrderSaveUrl = [NSString stringWithFormat:@"%@/appworkerorder/addSave.do",BASE_IP];
        _loginUrl = [NSString stringWithFormat:@"%@/appuser/addSave.do",BASE_IP];
        _smsCodeUrl = [NSString stringWithFormat:@"%@/appuser/sendSms.do",BASE_IP];
    }
    return self;
}

@end
