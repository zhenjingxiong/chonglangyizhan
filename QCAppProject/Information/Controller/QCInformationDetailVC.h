//
//  QCInformationDetailVC.h
//  QCAppProject
//
//  Created by jiale yang on 2021/8/21.
//

#import "QCBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QCInformationDetailVC : QCBaseViewController

@property (nonatomic, strong) NSString *detailLink;
@property (nonatomic, strong) NSString *mainTitle;
@property (nonatomic, strong) NSString *subTitle;

@end

NS_ASSUME_NONNULL_END
