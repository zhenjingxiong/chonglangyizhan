//
//  QCInformationViewController.m
//  QCAppProject
//
//  Created by yjl on 2021/8/16.
//

#import "QCInformationViewController.h"
#import "InformationHeadView.h"
#import "InformationTableViewCell.h"
#import "QCNetworkManager.h"
#import "QCInformationModel.h"
#import "QCInformationDetailVC.h"
#import "YFHRefresh.h"
#import <MJRefresh/MJRefresh.h>

@interface QCInformationViewController () <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *informationTableView;
@property (nonatomic, strong) InformationHeadView *headView;
@property (nonatomic, strong) NSArray *dataArray;
@property (nonatomic, strong) YFHRefresh *refreah;
@property (nonatomic, assign) CGFloat tableHeight;
@end

@implementation QCInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self loadData];
    [self configRefreshTable];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)initUI {
    self.title = @"热点资讯";
    if (self.navigationController.viewControllers.count > 1) {
        self.tableHeight = SCREEN_HEIGHT - kTopBarHeight;
    } else {
        self.tableHeight = SCREEN_HEIGHT - kTopBarHeight - kTabBarHeight;
    }
    [self.view addSubview:self.informationTableView];
    [self.informationTableView reloadData];
}

- (void)configRefreshTable {
    self.refreah = [[YFHRefresh alloc] init];
    WS(weakSelf)
    [self.refreah normalModelRefresh:self.informationTableView refreshType:(RefreshTypeDropDown) firstRefresh:NO timeLabHidden:NO stateLabHidden:NO dropDownBlock:^{
        if ([weakSelf.informationTableView.mj_header isRefreshing]) {
            [weakSelf.informationTableView.mj_header endRefreshing];
        }
        [self loadData];
    } upDropBlock:nil];
}

- (void)loadData {
    WS(weakSelf)
    if (self.navigationController.viewControllers.count > 1) {
        [QCProgressHUD showHUDWithWaitingMessage:@""];
    }
    [QCNetworkManager getRequestWithURL:[QCSingleton instance].URL.noticeUrl parameters:nil Progress:nil success:^(id  _Nonnull responseObject) {
        [QCProgressHUD hideHUD];
        QCLog(@"=====>>%@",responseObject);
        weakSelf.dataArray = [QCInformationModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
        [weakSelf.informationTableView reloadData];
    } failure:^(NSError * _Nonnull error) {
        QCLog(@"=====>>%@",error.description);
        [QCProgressHUD hideHUD];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    InformationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InformationTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell configCellData:self.dataArray[indexPath.row]];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    QCInformationDetailVC *detailVC = [QCInformationDetailVC new];
    QCInformationModel *model = self.dataArray[indexPath.row];
    detailVC.detailLink = model.htmltxt;
    detailVC.mainTitle = model.title;
    detailVC.subTitle = model.createDateStr;
    [self.navigationController pushViewController:detailVC animated:YES];
}


- (InformationHeadView *)headView {
    if (!_headView) {
        _headView = [[InformationHeadView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 32)];
    }
    return _headView;
}

- (UITableView *)informationTableView {
    if (!_informationTableView) {
        _informationTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,self.tableHeight) style:(UITableViewStylePlain)];
        _informationTableView.backgroundColor = [UIColor clearColor];
        _informationTableView.delegate = self;
        _informationTableView.dataSource = self;
        _informationTableView.tableHeaderView = self.headView;
        _informationTableView.estimatedRowHeight = 100;
        _informationTableView.estimatedSectionHeaderHeight = 0;
        _informationTableView.estimatedSectionFooterHeight = 0;
        _informationTableView.rowHeight = UITableViewAutomaticDimension;
        _informationTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_informationTableView registerClass:[InformationTableViewCell class] forCellReuseIdentifier:@"InformationTableViewCell"];
    }
    return _informationTableView;
}


- (NSArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSArray array];
    }
    return _dataArray;
}

@end
