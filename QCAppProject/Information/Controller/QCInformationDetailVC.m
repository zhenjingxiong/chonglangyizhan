//
//  QCInformationDetailVC.m
//  QCAppProject
//
//  Created by jiale yang on 2021/8/21.
//

#import "QCInformationDetailVC.h"
#import <WebKit/WebKit.h>
#import "WeakWebViewScriptMessageDelegate.h"

@interface QCInformationDetailVC () <WKNavigationDelegate>

@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UILabel *timeLbl;
@property (nonatomic, strong) WKWebView *wkDetailWeb;
@property (nonatomic, strong) UIScrollView *containerView;


@end

@implementation QCInformationDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [QCProgressHUD hideHUD];
}

- (void)initUI {
    self.title = @"资讯详情";
    [self.view addSubview:self.titleLbl];
    [self.view addSubview:self.timeLbl];
    _containerView=[[UIScrollView alloc] initWithFrame:(CGRectMake(12, self.timeLbl.bottom + 25, SCREEN_WIDTH-24, SCREEN_HEIGHT - kTopBarHeight - self.timeLbl.bottom - 25))];
    [self.view addSubview:_containerView];
}

- (void)initData {
    self.titleLbl.text = self.mainTitle;
    self.timeLbl.text = self.subTitle;
//    NSString *result = [NSString stringWithFormat:@"<%@ %@",@"img",@"style=\"width: 100%;height: 100%"];
    
    [QCProgressHUD showHUDWithWaitingMessage:@""];
    NSString *result = [NSString stringWithFormat:@"<%@ %@",@"img",@"style='display: block; max-width: 100%;'"];
    _wkDetailWeb = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH-24, 1)];
    _wkDetailWeb.navigationDelegate = self;
    NSString *str = [self flattenHTML:self.detailLink];
    [_wkDetailWeb loadHTMLString:[self getHtmlStrWithOrigin:[str stringByReplacingOccurrencesOfString:@"<img" withString:result]] baseURL:[NSURL URLWithString:@""]];
    _wkDetailWeb.scrollView.scrollEnabled=NO;
    _wkDetailWeb.hidden=YES;
    [_containerView addSubview:_wkDetailWeb];
}

//去掉html标签
- (NSString *)flattenHTML:(NSString *)html {
    NSScanner *theScanner;
    NSString *text = nil;
    theScanner = [NSScanner scannerWithString:html];
    while ([theScanner isAtEnd] == NO) {
        // find start of tag
        [theScanner scanUpToString:@"<table" intoString:NULL] ;
        // find end of tag
        [theScanner scanUpToString:@"table>" intoString:&text] ;
        // replace the found tag with a space
        //(you can filter multi-spaces out later if you wish)
        html=[html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@table>", text] withString:@""];
    }
    return html;
}

- (NSString *)getHtmlStrWithOrigin:(NSString *)originStr {
    NSString *htmlStr = [NSString stringWithFormat:@"<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/><meta content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1;\" name=\"viewport\" /><meta name=\"apple-mobile-web-app-capable\" content=\"yes\"><meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\"><link rel=\"stylesheet\" type=\"text/css\" /><style type=\"text/css\"> p{margin: 0px;} .color{color:#576b95;}</style></head><body><div id=\"content\">%@</div>", originStr];
    return htmlStr;
}

#pragma mark - WKNavigationDelegate
//页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
//    [QCProgressHUD showHUDWithWaitingMessage:@""];
}

// 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [QCProgressHUD hideHUD];
    _wkDetailWeb.hidden = NO;
    WS(weakSelf)
    [webView evaluateJavaScript:@"document.body.offsetHeight" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
        CGFloat webViewHeight = [result floatValue];
        QCLog(@"%@",@(webViewHeight));
        CGRect newFrame = webView.frame;
        newFrame.size.height = webViewHeight;
        webView.frame = newFrame;
        webView.height = webView.height + 20;
        weakSelf.containerView.contentSize = CGSizeMake(SCREEN_WIDTH - 24, webViewHeight + 20)  ;
    }];
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    
}

#pragma mark - Lazy

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(16, 22, SCREEN_WIDTH - 32, 0)];
        _titleLbl.font = FONT_PR(15);
        _titleLbl.textColor = QCBlack;
        _titleLbl.text = @"物联网的核心是什么?-创造高价值物联网的核心是什么?-";
        _titleLbl.numberOfLines = 0;
        [_titleLbl sizeToFit];
    }
    return _titleLbl;
}

- (UILabel *)timeLbl {
    if (!_timeLbl) {
        _timeLbl = [[UILabel alloc] initWithFrame:CGRectMake(16, self.titleLbl.bottom + 10, SCREEN_WIDTH - 32, 12)];
        _timeLbl.font = FONT_PR(11);
        _timeLbl.textColor = QCDesBlack;
        _timeLbl.text = @"2020-09-08 12:00";
    }
    return _timeLbl;
}


@end
