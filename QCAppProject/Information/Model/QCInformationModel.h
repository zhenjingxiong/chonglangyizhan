//
//  QCInformationModel.h
//  QCAppProject
//
//  Created by yjl on 2021/8/19.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QCInformationModel : NSObject

@property (nonatomic, strong) NSString *createDate;
@property (nonatomic, strong) NSString *createDateStr;
@property (nonatomic, strong) NSString *createUserId;
@property (nonatomic, strong) NSString *createUserName;
@property (nonatomic, strong) NSString *cId;
@property (nonatomic, strong) NSString *htmltxt;
@property (nonatomic, assign) NSInteger jsonUpdateFlag;
@property (nonatomic, assign) NSInteger mybatisRecordCount;
@property (nonatomic, strong) NSString *orderNo;
@property (nonatomic, strong) NSString *remark;
@property (nonatomic, strong) NSString *title;

@end

NS_ASSUME_NONNULL_END
