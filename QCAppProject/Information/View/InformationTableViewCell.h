//
//  InformationTableViewCell.h
//  QCAppProject
//
//  Created by yjl on 2021/8/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InformationTableViewCell : UITableViewCell

- (void)configCellData:(id)sender;

@end

NS_ASSUME_NONNULL_END
