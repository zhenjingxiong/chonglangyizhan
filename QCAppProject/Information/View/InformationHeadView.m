//
//  InformationHeadView.m
//  QCAppProject
//
//  Created by yjl on 2021/8/17.
//

#import "InformationHeadView.h"

@interface InformationHeadView ()

@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UILabel *subtitleLbl;
@end

@implementation InformationHeadView


- (instancetype)initWithFrame:(CGRect)frame {
    if (self == [super initWithFrame:frame]) {
        [self addSubview:self.titleLbl];
        [self.titleLbl setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
        [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@16);
            make.bottom.equalTo(self.mas_bottom);
        }];
        
        [self.subtitleLbl setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
        [self addSubview:self.subtitleLbl];
        [self.subtitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.titleLbl.mas_right).offset(8);
            make.centerY.equalTo(self.titleLbl.mas_centerY);
            make.right.equalTo(@-8);
        }];
    }
    return self;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _titleLbl.font = FONT_PR(13);
        _titleLbl.textColor = QCBlack;
        _titleLbl.text = @"热点消息";
        [_titleLbl sizeToFit];
    }
    return _titleLbl;
}

- (UILabel *)subtitleLbl {
    if (!_subtitleLbl) {
        _subtitleLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _subtitleLbl.font = FONT_PR(11);
        _subtitleLbl.textColor = QCTipBlack;
        _subtitleLbl.text = @"及时推送大V最新消息";
        [_subtitleLbl sizeToFit];
    }
    return _subtitleLbl;
}

@end
