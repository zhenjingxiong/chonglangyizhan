//
//  QCCooperationViewController.m
//  QCAppProject
//
//  Created by yjl on 2021/8/19.
//

#import "QCCooperationViewController.h"

@interface QCCooperationViewController ()

@property (nonatomic, strong) UIImageView *bgImageView;
@property (nonatomic, strong) NSMutableArray *labels;
@property (nonatomic, strong) NSArray *dataArray;
@property (nonatomic, strong) UILabel *nameLbl;
@property (nonatomic, strong) UILabel *phoneLbl;
@property (nonatomic, strong) UILabel *emailLbl;
@property (nonatomic, strong) UILabel *addressLbl;

@end

@implementation QCCooperationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initData];
}

- (UIImageView *)bgImageView {
    if (!_bgImageView) {
        _bgImageView = [[UIImageView alloc] initWithImage:fImageNamed(@"cooperation_bg")];
    }
    return _bgImageView;
}

- (void)initUI {
    self.title = @"商务合作";
    self.view.backgroundColor = QCBgColor;
    _labels = [NSMutableArray array];
    [self.view addSubview:self.bgImageView];
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.top.equalTo(@0);
        make.height.equalTo(@(SCREEN_WIDTH*0.44));
    }];
    [self.view addSubview:self.nameLbl];
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@10);
        make.top.equalTo(self.bgImageView.mas_bottom).offset(20);
        make.width.equalTo(@(SCREEN_WIDTH- 20));
    }];
    
    [self.view addSubview:self.phoneLbl];
    [self.phoneLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_left);
        make.top.equalTo(self.nameLbl.mas_bottom).offset(10);
        make.width.equalTo(@(SCREEN_WIDTH- 20));
    }];
    
    [self.view addSubview:self.emailLbl];
    [self.emailLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_left);
        make.top.equalTo(self.phoneLbl.mas_bottom).offset(10);
        make.width.equalTo(@(SCREEN_WIDTH- 20));
    }];
    
    [self.view addSubview:self.addressLbl];
    [self.addressLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_left);
        make.top.equalTo(self.emailLbl.mas_bottom).offset(10);
        make.width.equalTo(@(SCREEN_WIDTH- 20));
    }];
    [_labels addObject:self.nameLbl];
    [_labels addObject:self.phoneLbl];
    [_labels addObject:self.emailLbl];
    [_labels addObject:self.addressLbl];
}

- (void)initData {
    for (UILabel *lbl in _labels) {
        NSInteger index = lbl.tag - 10000;
        lbl.text = [NSString stringWithFormat:@"%@ %@" ,self.dataArray[index][@"key"],self.dataArray[index][@"value"]];
    }
}

- (NSArray *)dataArray{
    if (_dataArray == nil) {
        _dataArray = @[
                       @{
                           @"key":@"联系人:",
                           @"value":@"刘丽娜",
                           },
                       @{
                           @"key":@"电话:",
                           @"value":@"19925259674",
                           },
                       @{
                           @"key":@"邮箱:",
                           @"value":@"38738733@qq.com",
                           },
                       @{
                           @"key":@"地址:",
                           @"value":@"广东省深圳市宝安区西乡街道共乐社区铁仔路九方广场2栋1201",
                           },
                       ];
    }
    return _dataArray;
}

- (UILabel *)nameLbl {
    if (!_nameLbl) {
        _nameLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _nameLbl.font = FONT_PM(14);
        _nameLbl.textColor = QCBlack;
        _nameLbl.numberOfLines = 0 ;
        _nameLbl.tag = 10000;
        [_nameLbl sizeToFit];
    }
    return _nameLbl;
}

- (UILabel *)phoneLbl {
    if (!_phoneLbl) {
        _phoneLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _phoneLbl.font = FONT_PM(14);
        _phoneLbl.textColor = QCBlack;
        _phoneLbl.numberOfLines = 0 ;
        _phoneLbl.tag = 10001;
        [_phoneLbl sizeToFit];
    }
    return _phoneLbl;
}

- (UILabel *)emailLbl {
    if (!_emailLbl) {
        _emailLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _emailLbl.font = FONT_PM(14);
        _emailLbl.textColor = QCBlack;
        _emailLbl.numberOfLines = 0 ;
        _emailLbl.tag = 10002;
        [_emailLbl sizeToFit];
    }
    return _emailLbl;
}

- (UILabel *)addressLbl {
    if (!_addressLbl) {
        _addressLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _addressLbl.font = FONT_PM(14);
        _addressLbl.textColor = QCBlack;
        _addressLbl.numberOfLines = 0 ;
        _addressLbl.tag = 10003;
        [_addressLbl sizeToFit];
    }
    return _addressLbl;
}

@end
