//
//  QCPrivacyPolicyViewController.h
//  QCAppProject
//
//  Created by yjl on 2021/8/26.
//

#import "QCBaseViewController.h"

typedef void(^PrivacyPolicyJump)(void);

NS_ASSUME_NONNULL_BEGIN

@interface QCPrivacyPolicyViewController : QCBaseViewController

@property (nonatomic, copy) PrivacyPolicyJump privacyPolicyJump;

@end

NS_ASSUME_NONNULL_END
