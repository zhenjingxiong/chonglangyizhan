//
//  QCAdviceViewController.m
//  QCAppProject
//
//  Created by yjl on 2021/8/19.
//

#import "QCAdviceViewController.h"

static NSString *textPlaceholder = @"请填写您的建议~";
 
@interface QCAdviceViewController () <UITextViewDelegate>

@property (nonatomic, strong) UITextView *consultTextView;
@property (nonatomic, strong) UIButton *submitButton;
@property (nonatomic, strong) UILabel *accountLbl;
@property (nonatomic, strong) UILabel *accountDescLbl;

@end

@implementation QCAdviceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
}

- (void)initUI {
    self.title = @"投诉建议";
    self.view.backgroundColor = QCBgColor;
    [self.view addSubview:self.consultTextView];
    [self.consultTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.top.equalTo(@0);
        make.height.equalTo(@116);
    }];
    [self.view addSubview:self.submitButton];
    [self.submitButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@18);
        make.top.equalTo(self.consultTextView.mas_bottom).offset(43);
        make.width.equalTo(@(SCREEN_WIDTH - 36));
        make.height.equalTo(@40);
    }];
}

- (void)btnToClicked {
    
}

#pragma mark - UITextViewDelegate
- (void)textViewDidEndEditing:(UITextView *)textView {
    if(textView.text.length < 1){
        textView.text = textPlaceholder;
        textView.textColor = kHexColor(@"#A4A2A2");
    }
    
//    if (self.orderItemKey.count == 0) {
//        return;
//    }
//    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:textView.text,self.orderItemKey[self.tag - 20000], nil];
//    if (self.EditingBlock) {
//        self.EditingBlock(dict,self.tag);
//    }

}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if([textView.text isEqualToString:textPlaceholder]){
        textView.text = @"" ;
        textView.textColor = QCBlack;
    }
}

- (void)textViewEditChanged:(NSNotification *)obj {
    UITextView *textView = (UITextView *)obj.object;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]){ //判断输入的字是否是回车，即按下return
        [self.consultTextView resignFirstResponder];
        return NO; //这里返回NO，就代表return键值失效，即页面上按下return，不会出现换行，如果为yes，则输入页面会换行
    }
    
    return YES;
}

- (UITextView *)consultTextView {
    if (!_consultTextView) {
        _consultTextView= [[UITextView alloc] init];
        _consultTextView.backgroundColor = QCWhite;
        _consultTextView.font = FONT_PR(14);
        _consultTextView.textColor = kHexColor(@"#A4A2A2");
        _consultTextView.text = textPlaceholder;
        _consultTextView.returnKeyType = UIReturnKeyDone;
        _consultTextView.delegate=self;
        _consultTextView.textContainer.lineFragmentPadding = 18;
    }
    return _consultTextView;
}

- (UIButton *)submitButton {
    if (!_submitButton) {
        _submitButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _submitButton.titleLabel.font = FONT_PR(15);
        [_submitButton setTitle:@"确定" forState:(UIControlStateNormal)];
        [_submitButton setTitleColor:QCWhite forState:(UIControlStateNormal)];
        _submitButton.backgroundColor =  kHexColor(@"#FEB33E");
        _submitButton.layer.cornerRadius = 20.0f;
        [_submitButton addTarget:self action:@selector(btnToClicked) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _submitButton;
}

@end
