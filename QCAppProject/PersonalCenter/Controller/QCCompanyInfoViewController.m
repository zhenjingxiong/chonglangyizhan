//
//  QCCompanyInfoViewController.m
//  QCAppProject
//
//  Created by yjl on 2021/8/23.
//

#import "QCCompanyInfoViewController.h"
#import "QCNetworkManager.h"

@interface QCCompanyInfoViewController ()

@property(nonatomic,strong)UILabel *companyTitleLbl;
@property(nonatomic,strong)UILabel *companyDescLbl;
@property(nonatomic,strong)NSString *companyTitle;
@property(nonatomic,strong)NSString *companyDesc;

@end

@implementation QCCompanyInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self loadData];
}

-(void)setupUI{
    self.title = @"公司简介";
    self.view.backgroundColor = QCWhite;
    [self.view addSubview:self.companyTitleLbl];
    [self.companyTitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@(36));
        make.width.equalTo(@(SCREEN_WIDTH - 20));
        make.centerX.equalTo(self.view.mas_centerX);
    }];
    
    [self.view addSubview:self.companyDescLbl];
    [self.companyDescLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.companyTitleLbl.mas_bottom).offset(20);
        make.width.equalTo(@(SCREEN_WIDTH - 32));
        make.centerX.equalTo(self.view.mas_centerX);
    }];
}

- (void)loadData {
    WS(weakSelf)
    [QCNetworkManager getRequestWithURL:[QCSingleton instance].URL.companyInfoUrl parameters:nil Progress:nil success:^(id  _Nonnull responseObject) {
        QCLog(@"=====>>%@",responseObject);
        NSDictionary *dict = responseObject;
        if ([dict.allKeys containsObject:@"data"]) {
            NSDictionary *data = dict[@"data"];
            weakSelf.companyTitle = data[@"companyName"];
            weakSelf.companyDesc = data[@"companyDescp"];
            weakSelf.companyTitleLbl.text = weakSelf.companyTitle;
            weakSelf.companyDescLbl.text = weakSelf.companyDesc;
        }
    } failure:^(NSError * _Nonnull error) {
        QCLog(@"=====>>%@",error.description);
    }];
}

- (UILabel *)companyTitleLbl {
    if (!_companyTitleLbl) {
        _companyTitleLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _companyTitleLbl.font = FONT_PR(17);
        _companyTitleLbl.textColor = QCBlack;
        _companyTitleLbl.textAlignment = NSTextAlignmentCenter;
        _companyTitleLbl.text = @"";
        _companyTitleLbl.numberOfLines = 1;
        [_companyTitleLbl sizeToFit];
    }
    return _companyTitleLbl;
}

- (UILabel *)companyDescLbl {
    if (!_companyDescLbl) {
        _companyDescLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _companyDescLbl.font = FONT_PR(14);
        _companyDescLbl.textColor = QCSubBlack;
        _companyDescLbl.text = @"";
        _companyDescLbl.numberOfLines = 0;
        [_companyDescLbl sizeToFit];
    }
    return _companyDescLbl;
}


@end
