//
//  QCMineViewController.m
//  QCAppProject
//
//  Created by yjl on 2021/8/16.
//

#import "QCMineViewController.h"
#import "MineHeadView.h"
#import "MIneTableViewCell.h"
#import "NSString+Ext.h"
#import "QCLoginViewController.h"
#import "YFHUtil.h"
#import "QCWebViewController.h"
#import "QCNetworkManager.h"

@interface QCMineViewController () <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *mineTableView;
@property (nonatomic, strong) MineHeadView *headView;
@property (nonatomic, strong) NSArray *dataArray;

@end

@implementation QCMineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)initUI {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshAccount) name:@"login_success_refresh" object:nil];
    if (@available(iOS 11.0, *)) {
        self.mineTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.view addSubview:self.mineTableView];
    [self.mineTableView reloadData];
    
}

- (void)refreshAccount {
    [self.headView configHeadData];
}

- (void)pushKefuVC {
    WS(weakSelf)
    [QCNetworkManager getRequestWithURL:[QCSingleton instance].URL.queryKefuUrl parameters:nil Progress:nil success:^(id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dict = responseObject;
            if ([dict.allKeys containsObject:@"data"]) {
                NSDictionary *dict = responseObject[@"data"];
                if (dict && [dict.allKeys containsObject:@"link"]) {
                    NSString *link = dict[@"link"];
                    QCWebViewController *webVC = [QCWebViewController new];
                    webVC.url = link;
                    webVC.queryLinkType = QCQueryLinkFefu;
                    [weakSelf.navigationController pushViewController:webVC animated:YES];
                }
            }
        }
        QCLog(@"=====>>%@",responseObject);
    } failure:^(NSError *error) {
        QCLog(@"=====>>%@",error.description);
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MIneTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MIneTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.index = indexPath.row;
    NSDictionary * dict = self.dataArray[indexPath.row];
    [cell configCellData:dict];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    QCLog(@"%@",@(indexPath.row));
    NSDictionary * dict = self.dataArray[indexPath.row];
    if ([[dict objectForKey:@"cls"] isBlank]) {
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        NSString *registrationID = [[NSUserDefaults standardUserDefaults] objectForKey:kJpushRegistrationID];
        pasteboard.string = [YFHUtil isBlankString:registrationID] ? @"" : registrationID;
        return;
    }
    UIViewController * vc = [[NSClassFromString([dict objectForKey:@"cls"]) alloc] init];
    if ([vc isKindOfClass:[QCWebViewController class]]) {
        [self pushKefuVC];
        return;
    }
    [self.navigationController pushViewController:vc animated:YES];
}


- (MineHeadView *)headView {
    if (!_headView) {
        _headView = [[MineHeadView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 222)];
        WS(weakSelf)
        _headView.loginBlock = ^{
            if ([YFHUtil isBlankString:[QCSingleton instance].account] ) {
                QCLoginViewController *login = [[QCLoginViewController alloc] init];
                login.modalPresentationStyle = UIModalPresentationFullScreen;
                login.loginSuccessBlock = ^{
                    [weakSelf.headView configHeadData];
                };
                QCNavigationViewController *nav = [[QCNavigationViewController alloc] initWithRootViewController:login];
                [weakSelf presentViewController:nav animated:YES completion:nil];
            }
        };
    }
    return _headView;
}

- (UITableView *)mineTableView {
    if (!_mineTableView) {
        _mineTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:(UITableViewStylePlain)];
        _mineTableView.backgroundColor = [UIColor clearColor];
        _mineTableView.delegate = self;
        _mineTableView.dataSource = self;
        _mineTableView.tableHeaderView = self.headView;
        _mineTableView.rowHeight = 44;
        _mineTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_mineTableView registerClass:[MIneTableViewCell class] forCellReuseIdentifier:@"MIneTableViewCell"];
    }
    return _mineTableView;
}

- (NSArray *)dataArray{
    
    if (_dataArray == nil) {
        
        _dataArray = @[
                       @{
                           @"title":@"我的公告",
                           @"image":@"arrow_icon",
                           @"cls":@"QCInformationViewController"
                           },
                       @{
                           @"title":@"商务合作",
                           @"image":@"arrow_icon",
                           @"cls":@"QCCooperationViewController"
                           },
                       @{
                           @"title":@"在线客服",
                           @"image":@"arrow_icon",
                           @"cls":@"QCWebViewController"
                           },
                       @{
                           @"title":@"隐私政策",
                           @"image":@"arrow_icon",
                           @"cls":@"QCPrivacyPolicyViewController"
                           },
                       @{
                           @"title":@"公司简介",
                           @"image":@"arrow_icon",
                           @"cls":@"QCCompanyInfoViewController"
                           },
                       @{
                           @"title":@"项目版本",
                           @"image":@"",
                           @"cls":@""
                           }
                       ];
    }
    return _dataArray;
}



@end
