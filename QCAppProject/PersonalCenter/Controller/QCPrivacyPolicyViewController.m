//
//  QCPrivacyPolicyViewController.m
//  QCAppProject
//
//  Created by yjl on 2021/8/26.
//

#import "QCPrivacyPolicyViewController.h"
#import <WebKit/WebKit.h>

@interface QCPrivacyPolicyViewController ()<WKNavigationDelegate>

@property(nonatomic,strong)WKWebView*wkWebView;
@property(nonatomic,strong)UIProgressView*progressView;

@end

@implementation QCPrivacyPolicyViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
}


- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    if (self.privacyPolicyJump) {
        self.privacyPolicyJump();
    }
}

- (void)initUI {
    self.isShowBackButton = YES;
    self.title = @"隐私政策";
    self.wkWebView = [[WKWebView alloc] initWithFrame:self.view.bounds];
    self.wkWebView.frame=CGRectMake(0, 1, SCREEN_WIDTH, SCREEN_HEIGHT - kTopBarHeight);
    self.wkWebView.navigationDelegate=self;
    [self.wkWebView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
//    NSURL *fileURL = [[NSBundle mainBundle] URLForResource:@"用户协议.docx" withExtension:nil];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://iot.qichenglink.com/pages/ltd_ystk.html"]];
    [self.wkWebView loadRequest:request];
    [self.view addSubview:self.wkWebView];
    [self.view addSubview:self.progressView];
}

// 计算wkWebView进度条
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ( [keyPath isEqualToString:@"estimatedProgress"]) {
        CGFloat newprogress = [[change objectForKey:NSKeyValueChangeNewKey] doubleValue];
        self.progressView.alpha = 1.0f;
        [self.progressView setProgress:newprogress animated:YES];
        if (newprogress >= 1.0f) {
            [UIView animateWithDuration:0.3f
                                  delay:0.3f
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 self.progressView.alpha = 0.0f;
                             }
                             completion:^(BOOL finished) {
                                 [self.progressView setProgress:0 animated:NO];
                             }];
        }
        
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    //修改字体大小
//    NSString *fileUrl = [[NSBundle mainBundle] pathForResource:@"字体文件名" ofType:@"TTF"];
//    NSData *fileData = [NSData dataWithContentsOfFile:fileUrl];
//    NSString *boldFont = [fileData base64EncodedStringWithOptions:0];
//    NSMutableString *javascript = [NSMutableString string];
//    [javascript appendString:[NSString stringWithFormat:@"\
//                                  var boldcss = '@font-face { font-family: \"%@\"; src: url(data:font/ttf;base64,%@) format(\"truetype\");} *{font-family: \"%@\" !important;}'; \
//                                  var head = document.getElementsByTagName('head')[0], \
//                                  style = document.createElement('style'); \
//                                  style.type = 'text/css'; \
//                                  style.innerHTML = boldcss; \
//                                  head.appendChild(style);",@"字体文件对应的具体的字体名",boldFont,@"字体文件对应的具体的字体名"]];
//     [webView evaluateJavaScript:javascript completionHandler:nil];
}

- (UIProgressView *)progressView {
    if (!_progressView) {
        _progressView = [[UIProgressView alloc] init];
        _progressView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 1);
    }
    return _progressView;
}

- (void)dealloc {//最后别忘了销毁KVO监听
    [self.wkWebView removeObserver:self forKeyPath:@"estimatedProgress" context:nil];
    [self.wkWebView removeFromSuperview];
    self.wkWebView=nil;
    self.wkWebView.navigationDelegate=nil;
    
}
@end
