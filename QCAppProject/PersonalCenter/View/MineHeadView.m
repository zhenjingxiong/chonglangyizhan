//
//  MineHeadView.m
//  QCAppProject
//
//  Created by yjl on 2021/8/17.
//

#import "MineHeadView.h"
#import "UIView+ACSLayer.h"
#import "NSString+Ext.h"
#import "YFHUtil.h"

@interface MineHeadView ()

@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UIImageView *bgImageView;
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UIView *avaterBorderView;
@property (nonatomic, strong) UIImageView *avaterImageView;
@property (nonatomic, strong) UILabel *accountLbl;
@property (nonatomic, strong) UIView *contentView;

@property (nonatomic, strong) UIView *inviteView;
@property (nonatomic, strong) UILabel *inviteLbl;
@property (nonatomic, strong) UILabel *inviteSubLbl;

@property (nonatomic, strong) UIView *integralView;
@property (nonatomic, strong) UILabel *integralValueLbl;
@property (nonatomic, strong) UILabel *integralLbl;

@property (nonatomic, strong) UIView *signView;
@property (nonatomic, strong) UILabel *signLbl;
@property (nonatomic, strong) UILabel *signSubLbl;

@end

@implementation MineHeadView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self == [super initWithFrame:frame]) {
        [self initConstraints];
    }
    return self;
}

- (void)configHeadData {
    self.accountLbl.text = [YFHUtil isBlankString:[QCSingleton instance].account] ? @"未注册" :[[QCSingleton instance].account encryptedPhone];
}

- (void)registerBtnToClicked {
    if (self.loginBlock) {
        self.loginBlock();
    }
}

- (void)initConstraints {
    [self addSubview:self.containerView];
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self.containerView addSubview:self.bgImageView];
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.containerView);
    }];
    
    [self.containerView addSubview:self.titleLbl];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@16);
        make.top.equalTo(@40);
    }];
    
    [self.containerView addSubview:self.contentView];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@16);
        make.right.equalTo(@-16);
        make.bottom.equalTo(self.containerView.mas_bottom).offset(-10);
        make.height.equalTo(@127);
    }];
    [self.contentView acs_radiusWithRadius:2 corner:(UIRectCornerTopLeft|UIRectCornerTopRight|UIRectCornerBottomLeft|UIRectCornerBottomRight)];

    [self.containerView addSubview:self.avaterBorderView];
    [self.avaterBorderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@40);
        make.top.equalTo(self.contentView.mas_top).offset(-16);
        make.size.equalTo(@(CGSizeMake(70, 70)));
    }];
    
    [self.avaterBorderView addSubview:self.avaterImageView];
    [self.avaterImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.avaterBorderView.mas_centerX);
        make.centerY.equalTo(self.avaterBorderView.mas_centerY);
        make.size.equalTo(@(CGSizeMake(60, 60)));
    }];
    
    [self.containerView addSubview:self.accountLbl];
    [self.accountLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.avaterBorderView.mas_right).offset(12);
        make.centerY.equalTo(self.avaterBorderView.mas_centerY);
        make.width.equalTo(@120);
        make.height.equalTo(@(70));
    }];
    
    
    [self.contentView addSubview:self.inviteView];
    [self.inviteView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@0);
        make.top.equalTo(@58);
        make.height.equalTo(@48);
        make.width.equalTo(@((SCREEN_WIDTH - 32)/3));
    }];
    
    [self.inviteView addSubview:self.inviteLbl];
    [self.inviteLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.inviteView.mas_centerX);
        make.bottom.equalTo(self.inviteView.mas_centerY).offset(-5);
        make.width.equalTo(self.inviteView.mas_width);
    }];
    
    [self.inviteView addSubview:self.inviteSubLbl];
    [self.inviteSubLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.inviteView.mas_centerX);
        make.top.equalTo(self.inviteView.mas_centerY).offset(5);
        make.width.equalTo(self.inviteView.mas_width);
    }];
    
    [self.contentView addSubview:self.integralView];
    [self.integralView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView.mas_centerX);
        make.top.equalTo(self.inviteView.mas_top);
        make.height.equalTo(@48);
        make.width.equalTo(self.inviteView.mas_width);
    }];
    
    [self.integralView addSubview:self.integralValueLbl];
    [self.integralValueLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.integralView.mas_centerX);
        make.bottom.equalTo(self.integralView.mas_centerY).offset(-5);
        make.width.equalTo(self.integralView.mas_width);
    }];
    
    [self.integralView addSubview:self.integralLbl];
    [self.integralLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.integralView.mas_centerX);
        make.top.equalTo(self.integralView.mas_centerY).offset(5);
        make.width.equalTo(self.integralView.mas_width);
    }];
    
    [self.contentView addSubview:self.signView];
    [self.signView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@0);
        make.top.equalTo(self.inviteView.mas_top);
        make.height.equalTo(@48);
        make.width.equalTo(self.inviteView.mas_width);
    }];
    
    [self.signView addSubview:self.signLbl];
    [self.signLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.signView.mas_centerX);
        make.bottom.equalTo(self.signView.mas_centerY).offset(-5);
        make.width.equalTo(self.signView.mas_width);
    }];
    
    [self.signView addSubview:self.signSubLbl];
    [self.signSubLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.signView.mas_centerX);
        make.top.equalTo(self.signView.mas_centerY).offset(5);
        make.width.equalTo(self.signView.mas_width);
    }];
}


- (UIView *)containerView {
    if (!_containerView) {
        _containerView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return _containerView;
}

- (UIImageView *)bgImageView {
    if (!_bgImageView) {
        _bgImageView = [[UIImageView alloc] initWithImage:fImageNamed(@"mine_top_bg")];
    }
    return _bgImageView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _titleLbl.font = FONT_PR(15);
        _titleLbl.textColor = QCWhite;
        _titleLbl.text = @"个人中心";
        [_titleLbl sizeToFit];
    }
    return _titleLbl;
}

- (UIView *)contentView {
    if (!_contentView) {
        _contentView = [[UIView alloc] initWithFrame:CGRectZero];
        _contentView.backgroundColor = QCWhite;
    }
    return _contentView;
}

- (UIView *)avaterBorderView {
    if (!_avaterBorderView) {
        _avaterBorderView = [[UIView alloc] initWithFrame:CGRectZero];
        _avaterBorderView.layer.cornerRadius = 35.0f;
        _avaterBorderView.layer.masksToBounds = YES;
        _avaterBorderView.backgroundColor = QCWhite;
    }
    return _avaterBorderView;
}

- (UIImageView *)avaterImageView {
    if (!_avaterImageView) {
        _avaterImageView = [[UIImageView alloc] initWithImage:fImageNamed(@"avater_default")];
    }
    return _avaterImageView;
}

- (UILabel *)accountLbl {
    if (!_accountLbl) {
        _accountLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _accountLbl.userInteractionEnabled = YES;
        [_accountLbl addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(registerBtnToClicked)]];
        _accountLbl.font = FONT_PM(16);
        _accountLbl.textColor = QCBlack;
        _accountLbl.text = [YFHUtil isBlankString:[QCSingleton instance].account] ? @"未注册" :[[QCSingleton instance].account encryptedPhone];
        [_accountLbl sizeToFit];
    }
    return _accountLbl;
}

- (UIView *)inviteView {
    if (!_inviteView) {
        _inviteView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return _inviteView;
}

- (UILabel *)inviteLbl {
    if (!_inviteLbl) {
        _inviteLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _inviteLbl.font = FONT_PM(15);
        _inviteLbl.textAlignment = NSTextAlignmentCenter;
        _inviteLbl.textColor = QCBlack;
        _inviteLbl.text = @"邀请有礼";
        [_inviteLbl sizeToFit];
    }
    return _inviteLbl;
}

- (UILabel *)inviteSubLbl {
    if (!_inviteSubLbl) {
        _inviteSubLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _inviteSubLbl.font = FONT_PB(13);
        _inviteSubLbl.textAlignment = NSTextAlignmentCenter;
        _inviteSubLbl.textColor = kHexColor(@"#414A5D");
        _inviteSubLbl.text = @"多邀多得";
        [_inviteSubLbl sizeToFit];
    }
    return _inviteSubLbl;
}

- (UIView *)integralView {
    if (!_integralView) {
        _integralView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return _integralView;
}

- (UILabel *)integralValueLbl {
    if (!_integralValueLbl) {
        _integralValueLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _integralValueLbl.font = FONT_PM(15);
        _integralValueLbl.textAlignment = NSTextAlignmentCenter;
        _integralValueLbl.textColor = QCOrange;
        _integralValueLbl.text = @"暂无";
        [_integralValueLbl sizeToFit];
    }
    return _integralValueLbl;
}

- (UILabel *)integralLbl {
    if (!_integralLbl) {
        _integralLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _integralLbl.font = FONT_PB(13);
        _integralLbl.textAlignment = NSTextAlignmentCenter;
        _integralLbl.textColor = kHexColor(@"#414A5D");
        _integralLbl.text = @"我的积分";
        [_integralLbl sizeToFit];
    }
    return _integralLbl;
}

- (UIView *)signView {
    if (!_signView) {
        _signView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return _signView;
}

- (UILabel *)signLbl {
    if (!_signLbl) {
        _signLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _signLbl.font = FONT_PM(15);
        _signLbl.textAlignment = NSTextAlignmentCenter;
        _signLbl.textColor = QCBlack;
        _signLbl.text = @"立即签到";
        [_signLbl sizeToFit];
    }
    return _signLbl;
}

- (UILabel *)signSubLbl {
    if (!_signSubLbl) {
        _signSubLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _signSubLbl.font = FONT_PB(13);
        _signSubLbl.textAlignment = NSTextAlignmentCenter;
        _signSubLbl.textColor = kHexColor(@"#414A5D");
        _signSubLbl.text = @"得积分换权益";
        [_signSubLbl sizeToFit];
    }
    return _signSubLbl;
}

@end
