//
//  MIneTableViewCell.m
//  QCAppProject
//
//  Created by yjl on 2021/8/17.
//

#import "MIneTableViewCell.h"
#import "NSString+Ext.h"

@interface MIneTableViewCell ()

@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UIImageView *arrowImageView;
@property (nonatomic, strong) UILabel *subtitleLbl;

@end

@implementation MIneTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self == [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        UIView *superView = self.contentView;
        [superView addSubview:self.titleLbl];
        [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@24);
            make.centerY.equalTo(superView.mas_centerY);
        }];
        
        [superView addSubview:self.arrowImageView];
        [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(@-24);
            make.centerY.equalTo(superView.mas_centerY);
            make.size.equalTo(@(CGSizeMake(5, 8)));
        }];
        
        [superView addSubview:self.subtitleLbl];
        [self.subtitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(@-24);
            make.centerY.equalTo(superView.mas_centerY);
        }];
    }
    return self;
}

- (void)configCellData:(id)data {
    if (![data isKindOfClass:[NSDictionary class]]) {
        return;
    }
    self.titleLbl.text = data[@"title"];
    self.arrowImageView.image = fImageNamed(data[@"image"]);
    self.arrowImageView.hidden = [data[@"image"] isBlank];
    self.subtitleLbl.hidden = !self.arrowImageView.hidden;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _titleLbl.font = FONT_PR(16);
        _titleLbl.textColor = QCBlack;
        [_titleLbl sizeToFit];
    }
    return _titleLbl;
}

- (UILabel *)subtitleLbl {
    if (!_subtitleLbl) {
        _subtitleLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _subtitleLbl.font = FONT_PR(13);
        _subtitleLbl.textColor = QCTipBlack;
        _subtitleLbl.text = [NSString stringWithFormat:@"V%@",[NSString plistVersion]];
        _subtitleLbl.hidden = YES;
        [_subtitleLbl sizeToFit];
    }
    return _subtitleLbl;
}

- (UIImageView *)arrowImageView {
    if (!_arrowImageView) {
        _arrowImageView = [[UIImageView alloc] initWithImage:fImageNamed(@"arrow_icon")];
    }
    return _arrowImageView;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
