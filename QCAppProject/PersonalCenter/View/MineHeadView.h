//
//  MineHeadView.h
//  QCAppProject
//
//  Created by yjl on 2021/8/17.
//

#import <UIKit/UIKit.h>

typedef void(^LoginBlock)(void);

NS_ASSUME_NONNULL_BEGIN

@interface MineHeadView : UIView

@property (nonatomic , copy  ) LoginBlock loginBlock;

- (void)configHeadData;

@end

NS_ASSUME_NONNULL_END
