//
//  QCNavigationViewController.h
//  QCAppProject
//
//  Created by yjl on 2021/8/16.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QCNavigationViewController : UINavigationController


/**
 描述：是否支持滑动返回
 */
@property(nonatomic,assign)BOOL  isRecognizerBack;


@end

NS_ASSUME_NONNULL_END
