//
//  QCNavigationViewController.m
//  QCAppProject
//
//  Created by yjl on 2021/8/16.
//

#import "QCNavigationViewController.h"
#import "QCGradientLayerUtil.h"
#import "QCConsultViewController.h"
//#import "UINavigationBar+handle.h"

@interface QCNavigationViewController () <UIGestureRecognizerDelegate>

@end

@implementation QCNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [QCGradientLayerUtil configLayerGradientColor:self.navigationBar originColor:kHexColor(@"#FF5100") targetColor:kHexColor(@"#FFB15C")];
    self.modalPresentationStyle = UIModalPresentationFullScreen;
    self.navigationBar.tintColor = [UIColor whiteColor];

    
    self.interactivePopGestureRecognizer.enabled = YES;
    self.interactivePopGestureRecognizer.delegate = self;
    
    //默认开启滑动返回
    self.isRecognizerBack = true;
//    if (@available(iOS 13.0, *)) {
//        [self.navigationBar.standardAppearance configureWithTransparentBackground];
//        self.navigationBar.standardAppearance.shadowColor = [UIColor clearColor];
//    }else{
//        //导航栏透明
//        [self.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
//        //去掉下划线
//        self.navigationBar.shadowImage = [UIImage new];
//    }

//    [self.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
//    //去除 navigationBar 底部的细线
//    self.navigationBar.shadowImage = [UIImage new];
    
//    self.title = @"发答案的";
    
//    [self.navigationController.navigationBar navBarBackGroundColor:QCOrange image:nil isOpaque:YES];
}



- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
    if (self.viewControllers.count){
        viewController.hidesBottomBarWhenPushed = YES;
    }
    [super pushViewController:viewController animated:animated];
    if (![viewController isKindOfClass:[QCConsultViewController class]]) {
        [self setNavigationBarHidden:NO animated:YES];
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if (self.viewControllers.count <= 1 ) {
        return NO;
    }
    return self.isRecognizerBack;
}

+ (void)load{

//    UINavigationBar *navigationBar = [UINavigationBar appearance];
//    navigationBar.barTintColor = [UIColor whiteColor];
//    navigationBar.translucent = NO;
//    NSDictionary *parameter = @{
//                                NSForegroundColorAttributeName : [UIColor blackColor],
//                                NSFontAttributeName : [UIFont systemFontOfSize:17]
//                                };
//    [navigationBar setTitleTextAttributes:parameter];
}



- (void)back{
    [self popViewControllerAnimated:true];
}

@end
