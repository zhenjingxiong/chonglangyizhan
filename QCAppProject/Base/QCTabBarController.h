//
//  QCTabBarController.h
//  QCAppProject
//
//  Created by yjl on 2021/8/16.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QCTabBarController : UITabBarController

@end

NS_ASSUME_NONNULL_END
