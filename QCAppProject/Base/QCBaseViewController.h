//
//  QCBaseViewController.h
//  QCAppProject
//
//  Created by yjl on 2021/8/16.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QCBaseViewController : UIViewController

// 实例化Controller
- (id)initWithNibName:(NSString *)nibNameOrNil object:(id)object;

@property (nonatomic,retain) UIView *activtyTextField; //当前操作的对象;

@property (nonatomic,strong) UIButton *navBarRightBtn; //导航栏右侧按钮
// 是否显示返回按钮
@property (nonatomic,assign) BOOL isShowBackButton ;
// 返回标题
@property (nonatomic, copy) NSString *backTitle;

// 返回按钮
@property (nonatomic, strong) UIButton *backBut;

//隐藏导航栏黑线
- (void)hideNavBarHairline;

// 返回事件
- (void)backAction:(id)sender;

// 隐藏键盘
- (void)hideKeyboard;

//设置导航条标题
-(void)setNavBarTitle:(NSString *)title;

//设置右键
-(void)setNavBarRightBtnWithTitle:(NSString *)title andImageName:(NSString *)imgName;
//
//// 重设导航栏上按钮控件显示
//-(void)resetNavBarBtnsWithLeftBtnImg:(NSString *)leftBtnImg rigBtnImg:(NSString *)rigBtnImg;

//设置按钮透明度
-(void)resetBtnsAlpha:(CGFloat)alpha;

// 控制点击
- (void)setNavBarRightBtnEnabled:(BOOL)enabled;

//右键点击事件
-(void)navBarRightBtnAction:(id)sender;

//子类如果需要自己控制是否滑动返回，重写该方法,默认开启
-(BOOL)viewWillPopByGestureRecognizer:(UIGestureRecognizer*)gestureRecognizer;

-(void)setNavBarLeftBtnImg:(NSString *)imgName;

- (void)setHideNavBottomBlackLine:(BOOL)isHidden;

- (void)shareBtnHandled:(NSInteger)tag;

- (UIToolbar *)addToolbar;

-(void)setRightButton:(id)rightButton;

//空白页
- (void)setNoDataView:(NSString *)tipString imageName:(NSString *)imageName originY:(CGFloat)originY ;

- (void)hideNoDataView;

@end

NS_ASSUME_NONNULL_END
