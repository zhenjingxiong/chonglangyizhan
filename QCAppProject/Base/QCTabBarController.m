//
//  QCTabBarController.m
//  QCAppProject
//
//  Created by yjl on 2021/8/16.
//

#import "QCTabBarController.h"
#import "UIColor+HexColor.h"

#define kClassKey   @"rootVCClassString"
#define kTitleKey   @"title"
#define kImgKey     @"imageName"
#define kSelImgKey  @"selectedImageName"

@interface QCTabBarController ()

@end

@implementation QCTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    NSArray *childItemsArray = @[
                                 @{kClassKey  : @"QCHomeViewController",
                                   kTitleKey  : @"首页",
                                   kImgKey    : @"home_",
                                   kSelImgKey : @"home_"},
                                 
                                 @{kClassKey  : @"QCConsultViewController",
                                   kTitleKey  : @"问题咨询",
                                   kImgKey    : @"consult_",
                                   kSelImgKey : @"consult_"},
                                 
                                 @{kClassKey  : @"QCWebViewController",
                                   kTitleKey  : @"查询服务",
                                   kImgKey    : @"information_",
                                   kSelImgKey : @"information_"},
                                 
                                 @{kClassKey  : @"QCMineViewController",
                                   kTitleKey  : @"我的",
                                   kImgKey    : @"mine_",
                                   kSelImgKey : @"mine_"}];
    
    [childItemsArray enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
        UIViewController *vc = [NSClassFromString(dict[kClassKey]) new];
        vc.title = dict[kTitleKey];
        QCNavigationViewController *nav = [[QCNavigationViewController alloc]initWithRootViewController:vc];
        UITabBarItem *item = nav.tabBarItem;
        //        item.titlePositionAdjustment = UIOffsetMake(0, -4);
        item.imageInsets = UIEdgeInsetsMake(-2, 0, 2, 0);
        item.tag = 8000+idx;
        item.title = dict[kTitleKey];
        item.image = [[UIImage imageNamed:[NSString stringWithFormat:@"%@default",dict[kImgKey]]]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        item.selectedImage = [[UIImage imageNamed:[NSString stringWithFormat:@"%@pressed",dict[kSelImgKey]]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        if(@available(iOS 13.0, *)) {
            // iOS13 及以上
//            self.tabBar.tintColor = QCBlack;
//            self.tabBar.unselectedItemTintColor = QCOrange;
//            UITabBarItem *item = [UITabBarItem appearance];
            
            [item setTitleTextAttributes:@{NSFontAttributeName:FONT_PR(12.0f), NSForegroundColorAttributeName : QCBlack} forState:UIControlStateNormal];
            [item setTitleTextAttributes:@{NSFontAttributeName:FONT_PR(12.0f), NSForegroundColorAttributeName : QCOrange} forState:UIControlStateSelected];
        }
        else{
            // iOS13 以下
            UITabBarItem *item = [UITabBarItem appearance];
            [item setTitleTextAttributes:@{ NSForegroundColorAttributeName:QCBlack,NSFontAttributeName:FONT_PR(12.0f)} forState:UIControlStateNormal];
            [item setTitleTextAttributes:@{ NSForegroundColorAttributeName:QCOrange,NSFontAttributeName:FONT_PR(12.0f)} forState:UIControlStateSelected];
        }
//        [item setTitleTextAttributes:@{NSFontAttributeName:FONT_PR(12.0f), NSForegroundColorAttributeName : QCOrange} forState:UIControlStateSelected];
//        [item setTitleTextAttributes:@{NSFontAttributeName:FONT_PR(12.0f), NSForegroundColorAttributeName : QCBlack} forState:UIControlStateNormal];
        [self addChildViewController:nav];
    }];
    // Do any additional setup after loading the view.
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getTabbarIamge:) name:@"setTabbarImage" object:nil];
}

//-(void)getTabbarIamge:(NSNotification *)notification{
//    NSMutableArray *tabbarArray=notification.object;
//
//    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    app.tabbarImages = tabbarArray;
//    for (NSInteger i = 0; i < app.tabBarController.tabBar.subviews.count; i++) {
//        UIView *tabbar = app.tabBarController.tabBar.subviews[i];
//        if ([tabbar isKindOfClass:NSClassFromString(@"UITabBarButton")]) {
//            TabBarImageItem *item = nil;
//            if (tabbarArray.count > i - 1) {
//                item = [TabBarImageItem mj_objectWithKeyValues:tabbarArray[i-1]];
//            }
//
//            for (UIView * view in tabbar.subviews) {
//                NSLog(@"%@",[view class]);
//                YLog(@"-----------%@",@(app.tabBarController.selectedIndex));
//                if ([view isKindOfClass:NSClassFromString(@"UITabBarSwappableImageView")]) {
//                    // 在这里我们就创建图片了。如上边所说 uibutton的图片大小也是由图片去控制的。所以我们要创建一个imageView。
//                    UIImageView *tempImageView = [tabbar viewWithTag:50000+i];
//                    if (!tempImageView) {
//                        UIImageView * subView =  [[UIImageView alloc]initWithFrame:view.frame];
//                        subView.tag = 50000+i;
//                        if (i == app.tabBarController.selectedIndex + 1) {
//                            [subView sd_setImageWithURL:[NSURL URLWithString:item.afterImageUrl] placeholderImage:mImageNamed(@"")];
//                        } else {
//                            [subView sd_setImageWithURL:[NSURL URLWithString:item.preImageUrl] placeholderImage:mImageNamed(@"")];
//                        }
//
//                        // 在这里你可以加到这个imageView视图上。或者你可以加在tabbar里。因为加到这里 你方便去取。不然的话 你取出来更改图片的时候 还需要遍历一次。如果取出来的话 你还要去给imageview去设定一个tag值。
//                        [tabbar addSubview:subView];
//                    } else {
//                        if (i == app.tabBarController.selectedIndex + 1) {
//                            [tempImageView sd_setImageWithURL:[NSURL URLWithString:item.afterImageUrl] placeholderImage:mImageNamed(@"")];
//                        } else {
//                            [tempImageView sd_setImageWithURL:[NSURL URLWithString:item.preImageUrl] placeholderImage:mImageNamed(@"")];
//                        }
//                    }
//                    //这里需要设定原来的imageView隐藏 不然的话 两个图片会重叠。
//                    view.hidden = YES;
//                }
//            }
//        }
//
//    }
//}


//- (UIImage *)scaleToSize:(UIImage *)img size:(CGSize)size{
//    // 创建一个bitmap的context
//    // 并把它设置成为当前正在使用的context
//    UIGraphicsBeginImageContext(size);
//    // 绘制改变大小的图片
//    [img drawInRect:CGRectMake(0, 0, size.width, size.height)];
//    // 从当前context中创建一个改变大小后的图片
//    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    // 使当前的context出堆栈
//    UIGraphicsEndImageContext();
//    // 返回新的改变大小后的图片
//    return scaledImage;
//}


@end
