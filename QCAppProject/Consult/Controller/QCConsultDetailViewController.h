//
//  QCConsultDetailViewController.h
//  QCAppProject
//
//  Created by yjl on 2021/8/20.
//

#import "QCBaseViewController.h"

@class ConsultInfoModel;



NS_ASSUME_NONNULL_BEGIN

@interface QCConsultDetailViewController : QCBaseViewController

@property (nonatomic , strong) ConsultInfoModel *model;

@end

NS_ASSUME_NONNULL_END
