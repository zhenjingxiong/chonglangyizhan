//
//  ConsultSegmentViewController.m
//  QCAppProject
//
//  Created by yjl on 2021/8/17.
//

#import "ConsultSegmentViewController.h"
#import "UIView+ACSLayer.h"

@interface ConsultSegmentViewController ()

@property (nonatomic, assign, readwrite) NSInteger selectedIndex;

@end

@implementation ConsultSegmentViewController

#pragma mark - Lifecycle

- (instancetype)initWithViewControllers:(NSArray *)viewControllers frame:(CGRect)frame {
    if (self = [super init]) {
        self.view.frame = frame;
        [self initLayout:viewControllers];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)setSelectedIndex:(NSInteger)selectedIndex {
    if (_selectedIndex == selectedIndex) {
        return;
    }
    
    UIViewController *oldController = [_viewControllers objectAtIndex:_selectedIndex];
    UIViewController *newController = [_viewControllers objectAtIndex:selectedIndex];
    [self addChildViewController:newController];
    [self transitionFromViewController:oldController toViewController:newController duration:0 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:^(BOOL finished) {
        if (finished) {
            [newController didMoveToParentViewController:self];
            [oldController willMoveToParentViewController:nil];
            [oldController removeFromParentViewController];
            
            [newController.view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(self.view).insets(UIEdgeInsetsZero);
            }];
        }
    }];
    _selectedIndex = selectedIndex;
}

- (void)initLayout:(NSArray *)viewControllers {
//    self.view.layer.cornerRadius = 10.0f;
//    self.view.layer.masksToBounds = YES;
    _viewControllers = [viewControllers copy];
    _selectedIndex = 0;
    UIViewController *firstVC = viewControllers[_selectedIndex];
    [self addChildViewController:firstVC];
    [self.view addSubview:firstVC.view];
    [firstVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.and.bottom.right.equalTo(@0);
        //        make.edges.equalTo(self.view).insets(UIEdgeInsetsZero);
    }];
}



@end
