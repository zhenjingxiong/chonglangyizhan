//
//  ConsultChildViewController.m
//  QCAppProject
//
//  Created by yjl on 2021/8/18.
//

#import "ConsultChildViewController.h"
#import "ConsultTableViewCell.h"
#import "UIView+ACSLayer.h"
#import "QCNetworkManager.h"
#import "ConsultInfoModel.h"
#import "YFHRefresh.h"
#import <MJRefresh/MJRefresh.h>
#import "QCConsultDetailViewController.h"
#import "QCSubmitConsultVC.h"

@interface ConsultChildViewController () <UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UITableView *consultTableView;

@property (nonatomic, strong) UIButton *consultBtn;

@property (nonatomic, strong) NSArray *dataArray;

@property (nonatomic, strong) YFHRefresh *refreah;

@property (nonatomic,strong) UILabel *defaultLabel;//缺省页控件

@end

@implementation ConsultChildViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.consultTableView];
    [self.consultTableView reloadData];
    [self.view addSubview:self.consultBtn];
    [self.consultBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(10));
        make.top.equalTo(@(SCREEN_HEIGHT - kTabBarHeight - 168 - 36 - 40));
        make.width.equalTo(@(SCREEN_WIDTH - 44));
        make.height.equalTo(@40);
    }];
    
    [self configRefreshTable];
    [self loadConsultListData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)configRefreshTable {
    self.refreah = [[YFHRefresh alloc] init];
    WS(weakSelf)
    [self.refreah normalModelRefresh:self.consultTableView refreshType:(RefreshTypeDropDown) firstRefresh:NO timeLabHidden:NO stateLabHidden:NO dropDownBlock:^{
        if ([weakSelf.consultTableView.mj_header isRefreshing]) {
            [weakSelf.consultTableView.mj_header endRefreshing];
        }
        [self loadConsultListData];
    } upDropBlock:nil];
}

- (void)loadConsultListData {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:@(1) forKey:@"wstatus"];
    [dict setValue:[QCSingleton instance].account forKey:@"userPhone"];
    WS(weakSelf)
    [QCNetworkManager getRequestWithURL:[QCSingleton instance].URL.workerOrderUrl parameters:dict Progress:nil success:^(id  _Nonnull responseObject) {
        QCLog(@"=====>>%@",responseObject);
        weakSelf.dataArray = [ConsultInfoModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
        [weakSelf.consultTableView reloadData];
        [self showDefaultPage];
    } failure:^(NSError * _Nonnull error) {
        QCLog(@"=====>>%@",error.description);
    }];
}

- (void)showDefaultPage {
    if(self.dataArray.count == 0){
        [self.view addSubview:self.defaultLabel];
        self.defaultLabel.hidden = NO;
        self.consultTableView.hidden = YES;
    } else{
        self.consultTableView.hidden = NO;
        self.defaultLabel.hidden = YES;
    }
}

- (void)btnToClicked {
    QCLog(@"咨询问题");
    QCSubmitConsultVC *submitConsultVC = [QCSubmitConsultVC new];
    submitConsultVC.submitBlock = ^{
        [self loadConsultListData];
    };
    [self.navigationController pushViewController:submitConsultVC animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ConsultTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ConsultTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell configCellData:self.dataArray[indexPath.row]];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    QCConsultDetailViewController *detailVC = [QCConsultDetailViewController new];
    detailVC.model = self.dataArray[indexPath.row];
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (UITableView *)consultTableView {
    if (!_consultTableView) {
        _consultTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 24, SCREEN_HEIGHT - kTabBarHeight - 168 - 80) style:(UITableViewStylePlain)];
        _consultTableView.backgroundColor = [UIColor clearColor];
        _consultTableView.delegate = self;
        _consultTableView.dataSource = self;
        _consultTableView.estimatedRowHeight = 100;
        _consultTableView.estimatedSectionHeaderHeight = 0;
        _consultTableView.estimatedSectionFooterHeight = 0;
        _consultTableView.rowHeight = UITableViewAutomaticDimension;
        _consultTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_consultTableView registerClass:[ConsultTableViewCell class] forCellReuseIdentifier:@"ConsultTableViewCell"];
        [_consultTableView acs_radiusWithRadius:10.0f corner:(UIRectCornerTopLeft|UIRectCornerTopRight)];
    }
    return _consultTableView;
}

- (UIButton *)consultBtn {
    if (!_consultBtn) {
        _consultBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _consultBtn.titleLabel.font = FONT_PR(15);
        [_consultBtn setTitle:@"问题咨询" forState:(UIControlStateNormal)];
        [_consultBtn setTitleColor:QCWhite forState:(UIControlStateNormal)];
        _consultBtn.backgroundColor =  kHexColor(@"#FEB33E");
        _consultBtn.layer.cornerRadius = 20.0f;
        [_consultBtn addTarget:self action:@selector(btnToClicked) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _consultBtn;
}

- (UILabel *)defaultLabel {
    if (!_defaultLabel){
        _defaultLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, kTopBarHeight + 30, SCREEN_WIDTH - 24, 20)];
        _defaultLabel.textAlignment = NSTextAlignmentCenter;
        _defaultLabel.font = [UIFont systemFontOfSize:14];
        _defaultLabel.text = @"暂无数据";
        _defaultLabel.textColor = kHexColor(@"#9EA0A3");
    }
    return _defaultLabel;
}

@end
