//
//  ConsultChildViewController.h
//  QCAppProject
//
//  Created by yjl on 2021/8/18.
//

#import "QCBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ConsultChildViewController : QCBaseViewController

@property (nonatomic, assign) BOOL isSecondLevel;

@end

NS_ASSUME_NONNULL_END
