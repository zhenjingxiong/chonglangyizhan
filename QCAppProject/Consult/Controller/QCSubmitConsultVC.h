//
//  QCSubmitConsultVC.h
//  QCAppProject
//
//  Created by yjl on 2021/8/18.
//

#import "QCBaseViewController.h"

typedef void(^SubmitBlock)(void);

NS_ASSUME_NONNULL_BEGIN

@interface QCSubmitConsultVC : QCBaseViewController

@property (nonatomic , copy  ) SubmitBlock submitBlock;

@end

NS_ASSUME_NONNULL_END
