//
//  ConsultChild2ViewController.h
//  QCAppProject
//
//  Created by yjl on 2021/8/20.
//

#import "QCBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ConsultChild2ViewController : QCBaseViewController


@property (nonatomic, assign) BOOL isSecondLevel;


@end

NS_ASSUME_NONNULL_END
