//
//  QCConsultDetailViewController.m
//  QCAppProject
//
//  Created by yjl on 2021/8/20.
//

#import "QCConsultDetailViewController.h"
#import "QCGradientLayerUtil.h"
#import "ConsultInfoModel.h"

@interface QCConsultDetailViewController ()

@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIView *noticeView;

@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UILabel *subtitleLbl;
@property (nonatomic, strong) UILabel *timeLbl;

@property (nonatomic, strong) UIView *replyContentView;
@property (nonatomic, strong) UILabel *replyTitleLbl;
@property (nonatomic, strong) UILabel *replyContentLbl;
@property (nonatomic, strong) UILabel *replyTimeLbl;


@end

@implementation QCConsultDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"咨询详情";
    self.view.backgroundColor = QCBgColor;
    [self initConstraints];
    [QCGradientLayerUtil configLayerGradientColor:self.navigationController.navigationBar originColor:kHexColor(@"#FF5100") targetColor:kHexColor(@"#EF8B46")];
    [self initData];
}

- (void)initConstraints {
    [self.view addSubview:self.bgView];
    [QCGradientLayerUtil configLayerGradientColor:self.bgView originColor:kHexColor(@"#EF8B46") targetColor:kHexColor(@"#FFB15C")];
    [self.bgView addSubview:self.noticeView];
    
    [self.view addSubview:self.contentView];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(@0);
        make.top.equalTo(@(self.bgView.bottom));
    }];
    
    [self.contentView addSubview:self.titleLbl];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@20);
        make.top.equalTo(@(35));
        make.width.equalTo(@(SCREEN_WIDTH - 40 - 120));
    }];
    
    [self.contentView addSubview:self.timeLbl];
    [self.timeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@-20);
        make.top.equalTo(self.titleLbl.mas_top).offset(3);
        make.width.equalTo(@120);
    }];
    
    [self.contentView addSubview:self.subtitleLbl];
    [self.subtitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLbl.mas_left);
        make.top.equalTo(self.titleLbl.mas_bottom).offset(12);
        make.width.equalTo(@(SCREEN_WIDTH - 40));
    }];
    
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.subtitleLbl.mas_bottom).offset(25);
    }];
    
    [self.view addSubview:self.replyContentView];
    [self.replyContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(@0);
        make.top.equalTo(self.contentView.mas_bottom).offset(8);
    }];

    [self.replyContentView addSubview:self.replyTitleLbl];
    [self.replyTitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@20);
        make.top.equalTo(@(35));
        make.width.equalTo(@(SCREEN_WIDTH - 40 - 100));
    }];

    [self.replyContentView addSubview:self.replyTimeLbl];
    [self.replyTimeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@-20);
        make.top.equalTo(self.replyTitleLbl.mas_top).offset(3);
        make.width.equalTo(@100);
    }];

    [self.replyContentView addSubview:self.replyContentLbl];
    [self.replyContentLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.replyTitleLbl.mas_left);
        make.top.equalTo(self.replyTitleLbl.mas_bottom).offset(12);
        make.width.equalTo(@(SCREEN_WIDTH - 40));
    }];

    [self.replyContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.replyContentLbl.mas_bottom).offset(25);
    }];

}

- (void)initData {
    self.subtitleLbl.text = self.model.content;
    self.timeLbl.text = self.model.createDateStr;
    self.replyContentLbl.text = self.model.reply;
    self.replyTimeLbl.text = self.model.handleDateStr;
    if (self.model.reply && self.model.reply.length > 0) {
        self.replyContentView.hidden = NO;
    } else {
        self.replyContentView.hidden = YES;
    }
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
        _bgView.backgroundColor = kHexColor(@"FFB15C");
    }
    return _bgView;
}

- (UIView *)noticeView {
    if (!_noticeView) {
        _noticeView = [[UIView alloc] initWithFrame:CGRectMake(16, 4, SCREEN_WIDTH - 32, 24)];
        _noticeView.backgroundColor = kHexColorAlpha(@"#FFECDD", 0.3);;
        _noticeView.layer.cornerRadius = 12.0f;
        UIImageView *noticeImage = [[UIImageView alloc] initWithImage:fImageNamed(@"home_notice")];
        noticeImage.frame =CGRectMake(10, 5, 12, 14);
        [_noticeView addSubview:noticeImage];
        UILabel *titleLbl = [[UILabel alloc] initWithFrame:self.noticeView.bounds];
        titleLbl.font = FONT_PR(11);
        titleLbl.text = @"您的问题我们正在积极处理中哦～";
        titleLbl.numberOfLines = 1;
        titleLbl.textColor = QCWhite;
        titleLbl.left = noticeImage.right + 8;
        titleLbl.width = _noticeView.width - 40;
        [_noticeView addSubview:titleLbl];
    }
    return _noticeView;
}

- (UIView *)contentView {
    if (!_contentView) {
        _contentView = [[UIView alloc] initWithFrame:CGRectZero];
        _contentView.backgroundColor = QCWhite;
    }
    return _contentView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _titleLbl.font = FONT_PR(15);
        _titleLbl.textColor = QCBlack;
        _titleLbl.text = @"咨询问题";
        _titleLbl.numberOfLines = 0;
        [_titleLbl sizeToFit];//自适应，在这可写可不写，一般在frame布局时，设置width没有设置高度，调用此方法可自动适应高度
    }
    return _titleLbl;
}

- (UILabel *)subtitleLbl {
    if (!_subtitleLbl) {
        _subtitleLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _subtitleLbl.font = FONT_PR(13);
        _subtitleLbl.textColor = QCSubBlack;
        _subtitleLbl.text = @"";
        _subtitleLbl.numberOfLines = 0;
        [_subtitleLbl sizeToFit];
    }
    return _subtitleLbl;
}

- (UILabel *)timeLbl {
    if (!_timeLbl) {
        _timeLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _timeLbl.font = FONT_PR(11);
        _timeLbl.textColor = QCDesBlack;
        [_timeLbl sizeToFit];
    }
    return _timeLbl;
}

- (UIView *)replyContentView {
    if (!_replyContentView) {
        _replyContentView = [[UIView alloc] initWithFrame:CGRectZero];
        _replyContentView.backgroundColor = QCWhite;
    }
    return _replyContentView;
}

- (UILabel *)replyTitleLbl {
    if (!_replyTitleLbl) {
        _replyTitleLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _replyTitleLbl.font = FONT_PR(15);
        _replyTitleLbl.textColor = QCBlack;
        _replyTitleLbl.text = @"客服回复";
        _replyTitleLbl.numberOfLines = 0;
        [_replyTitleLbl sizeToFit];
    }
    return _replyTitleLbl;
}

- (UILabel *)replyContentLbl {
    if (!_replyContentLbl) {
        _replyContentLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _replyContentLbl.font = FONT_PR(13);
        _replyContentLbl.textColor = QCSubBlack;
        _replyContentLbl.text = @"";
        _replyContentLbl.numberOfLines = 0;
        [_replyContentLbl sizeToFit];
    }
    return _replyContentLbl;
}

- (UILabel *)replyTimeLbl {
    if (!_replyTimeLbl) {
        _replyTimeLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _replyTimeLbl.font = FONT_PR(11);
        _replyTimeLbl.textColor = QCDesBlack;
        [_replyTimeLbl sizeToFit];
    }
    return _replyTimeLbl;
}

@end
