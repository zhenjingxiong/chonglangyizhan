//
//  ConsultSegmentViewController.h
//  QCAppProject
//
//  Created by yjl on 2021/8/17.
//

#import "QCBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ConsultSegmentViewController : QCBaseViewController

@property (nonatomic, strong) NSArray *viewControllers;

- (instancetype)initWithViewControllers:(NSArray *)viewControllers frame:(CGRect)frame;

- (void)setSelectedIndex:(NSInteger)selectedIndex;

@end

NS_ASSUME_NONNULL_END
