//
//  QCConsultViewController.m
//  QCAppProject
//
//  Created by yjl on 2021/8/16.
//

#import "QCConsultViewController.h"
#import "ConsultTopView.h"
#import "PPiFlatSegmentedControl.h"
#import "ConsultSegmentViewController.h"
#import "UIView+ACSLayer.h"
#import "UISegmentedControl+Category.h"
#import "ConsultChildViewController.h"
#import "ConsultChild2ViewController.h"
#import "QCSubmitConsultVC.h"
#import "YFHUtil.h"
#import "QCGradientLayerUtil.h"

const CGFloat consultTopHeight = 278.0;

@interface QCConsultViewController ()

@property (nonatomic, strong) ConsultTopView *topView;

@property (nonatomic, strong) UISegmentedControl    *segmentedControl ;
@property (nonatomic, strong) ConsultSegmentViewController *detailSegmentVC;
@property (nonatomic, assign) BOOL isFirstLevelPage;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UILabel *userAccountLbl;
@end

@implementation QCConsultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [self.navigationController setNavigationBarHidden:YES animated:self.isFirstLevelPage ? animated : NO];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)backBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)initUI {
    self.view.backgroundColor = QCBgColor;
    self.isFirstLevelPage = YES;
    if ([self.navigationController.viewControllers count] > 1) {
        self.isFirstLevelPage = NO;
        self.backButton.hidden = NO;
    } else {
        self.userAccountLbl.left = 16;
    }
    [self.view addSubview:self.topView];
    [self.view addSubview:self.backButton];
    [self.view addSubview:self.userAccountLbl];
    self.userAccountLbl.text = [NSString stringWithFormat:@"%@用户",[QCSingleton instance].account];
    [self.view addSubview:self.segmentedControl];
    [self.segmentedControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(28);
        make.right.equalTo(self.view.mas_right).offset(-28);
        make.top.equalTo(self.view.mas_top).offset(128);
        make.height.equalTo(@38);
    }];

    [self addChildViewController:self.detailSegmentVC];
    [self.view addSubview:self.detailSegmentVC.view];
    [self.detailSegmentVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.segmentedControl.mas_bottom).offset(20);
        make.left.equalTo(@12);
        make.right.equalTo(@-12);
        make.bottom.equalTo(@0);
    }];
    [self.detailSegmentVC.view acs_radiusWithRadius:10.0f corner:(UIRectCornerTopLeft|UIRectCornerTopRight)];
}

- (NSArray *)detailSegs {
    return  @[
               @"待处理",
               @"已处理"];
}

- (UISegmentedControl *)segmentedControl {
    if (!_segmentedControl) {
        _segmentedControl = [[UISegmentedControl alloc] initWithItems:[self detailSegs]];
        _segmentedControl.selectedSegmentIndex = 0;
        _segmentedControl.backgroundColor = QCWhite;
        [_segmentedControl addTarget:self action:@selector(switchToSegmentView) forControlEvents:(UIControlEventValueChanged)];
        _segmentedControl.tintColor = QCWhite;
        _segmentedControl.backgroundColor = [UIColor clearColor];
        [_segmentedControl segmentedIOS13Style];
        [_segmentedControl setTitleTextAttributes:@{NSForegroundColorAttributeName:QCWhite ,NSFontAttributeName:FONT_PR(15)} forState:UIControlStateNormal];
        [_segmentedControl setTitleTextAttributes:@{NSForegroundColorAttributeName:QCBlack,NSFontAttributeName:FONT_PR(15)}forState:UIControlStateSelected];
    }
    return _segmentedControl;
}

- (void)switchToSegmentView {
    NSLog(@"%@",@(self.segmentedControl.selectedSegmentIndex));
    [self.detailSegmentVC setSelectedIndex:self.segmentedControl.selectedSegmentIndex];
}

- (ConsultSegmentViewController *)detailSegmentVC {
    if (!_detailSegmentVC) {
        ConsultChildViewController *vc1 = [ConsultChildViewController new];
        vc1.view.backgroundColor = [UIColor whiteColor];
        vc1.isSecondLevel = ! self.isFirstLevelPage;
        [vc1.view acs_radiusWithRadius:10.0f corner:(UIRectCornerTopLeft|UIRectCornerTopRight)];
        ConsultChild2ViewController *vc2 = [ConsultChild2ViewController new];
        vc2.view.backgroundColor = [UIColor whiteColor];
        vc2.isSecondLevel = ! self.isFirstLevelPage;
        [vc2.view acs_radiusWithRadius:10.0f corner:(UIRectCornerTopLeft|UIRectCornerTopRight)];
        CGRect rect = CGRectMake(0, 0 , SCREEN_WIDTH, 0);
        _detailSegmentVC = [[ConsultSegmentViewController alloc] initWithViewControllers:@[vc1,vc2] frame:rect];
    }
    return _detailSegmentVC;
}

- (ConsultTopView *)topView {
    if (!_topView) {
        _topView = [[ConsultTopView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,consultTopHeight)];
    }
    return _topView;
}

- (UIButton *)backButton {
    if (!_backButton) {
        _backButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _backButton.frame = CGRectMake(0, kStatusBarHeight, 60, 40);
        [_backButton setImage:fImageNamed(@"ic_back_24pt_white") forState:(UIControlStateNormal)];
        [_backButton addTarget:self action:@selector(backBtnAction) forControlEvents:(UIControlEventTouchUpInside)];
        _backButton.hidden = YES;
    }
    return _backButton;
}

- (UILabel *)userAccountLbl {
    if (!_userAccountLbl) {
        _userAccountLbl = [[UILabel alloc] initWithFrame:CGRectMake(self.backBut.right, kStatusBarHeight, 200, 40)];
        _userAccountLbl.font = FONT_PR(15);
        _userAccountLbl.textAlignment = NSTextAlignmentLeft;
        _userAccountLbl.textColor = QCWhite;
    }
    return _userAccountLbl;
}

@end
