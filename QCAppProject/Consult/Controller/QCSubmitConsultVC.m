//
//  QCSubmitConsultVC.m
//  QCAppProject
//
//  Created by yjl on 2021/8/18.
//

#import "QCSubmitConsultVC.h"
#import "NSString+Ext.h"
#import "QCNetworkManager.h"

static NSString *textPlaceholder = @"请简短描述您的问题~";

@interface QCSubmitConsultVC () <UITextViewDelegate>

@property (nonatomic, strong) UITextView *consultTextView;
@property (nonatomic, strong) UIButton *submitButton;
@property (nonatomic, strong) UILabel *accountLbl;
@property (nonatomic, strong) UILabel *accountDescLbl;

@end

@implementation QCSubmitConsultVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
}

- (void)initUI {
    self.title = @"问题咨询";
    self.view.backgroundColor = QCBgColor;
    [self.view addSubview:self.consultTextView];
    [self.consultTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.top.equalTo(@0);
        make.height.equalTo(@116);
    }];
    [self.view addSubview:self.submitButton];
    [self.submitButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@18);
        make.top.equalTo(self.consultTextView.mas_bottom).offset(43);
        make.width.equalTo(@(SCREEN_WIDTH - 36));
        make.height.equalTo(@40);
    }];
    
//    [self.view addSubview:self.accountLbl];
//    [self.accountLbl mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.equalTo(self.view.mas_centerX);
//        make.top.equalTo(self.submitButton.mas_bottom).offset(45);
//    }];
//
//    [self.view addSubview:self.accountDescLbl];
//    [self.accountDescLbl mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.accountLbl.mas_right);
//        make.centerY.equalTo(self.accountLbl.mas_centerY);
//    }];
}

- (void)btnToClicked {
    if ([[self.consultTextView.text removeWhiteSpacesFromString] isEqualToString:textPlaceholder]) {
        [QCProgressHUD showHUDWithMessage:@"请输入您的问题～" autoHide:YES];
        return;;
    }
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:[QCSingleton instance].account forKey:@"userPhone"];
    [dict setValue:self.consultTextView.text forKey:@"content"];
    [QCNetworkManager getRequestWithURL:[QCSingleton instance].URL.workerOrderSaveUrl parameters:dict Progress:nil success:^(id responseObject) {
        NSLog(@"%@",responseObject);
        if (self.submitBlock) {
            self.submitBlock();
        }
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(NSError *error) {
        NSLog(@"%@",error.description);
    }];
    
}

#pragma mark - UITextViewDelegate
- (void)textViewDidEndEditing:(UITextView *)textView {
    if(textView.text.length < 1){
        textView.text = textPlaceholder;
        textView.textColor = kHexColor(@"#A4A2A2");
    }

}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if([textView.text isEqualToString:textPlaceholder]){
        textView.text = @"" ;
        textView.textColor = QCBlack;
    }
}

- (void)textViewEditChanged:(NSNotification *)obj {
    UITextView *textView = (UITextView *)obj.object;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]){ //判断输入的字是否是回车，即按下return
        [self.consultTextView resignFirstResponder];
        return NO; //这里返回NO，就代表return键值失效，即页面上按下return，不会出现换行，如果为yes，则输入页面会换行
    }
    
    return YES;
}

- (UITextView *)consultTextView {
    if (!_consultTextView) {
        _consultTextView= [[UITextView alloc] init];
        _consultTextView.backgroundColor = QCWhite;
        _consultTextView.font = FONT_PR(14);
        _consultTextView.textColor = kHexColor(@"#A4A2A2");
        _consultTextView.text = textPlaceholder;
        _consultTextView.returnKeyType = UIReturnKeyDone;
        _consultTextView.delegate=self;
        _consultTextView.textContainer.lineFragmentPadding = 18;
    }
    return _consultTextView;
}

- (UIButton *)submitButton {
    if (!_submitButton) {
        _submitButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _submitButton.titleLabel.font = FONT_PR(15);
        [_submitButton setTitle:@"确定" forState:(UIControlStateNormal)];
        [_submitButton setTitleColor:QCWhite forState:(UIControlStateNormal)];
        _submitButton.backgroundColor =  kHexColor(@"#FEB33E");
        _submitButton.layer.cornerRadius = 20.0f;
        [_submitButton addTarget:self action:@selector(btnToClicked) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _submitButton;
}

- (UILabel *)accountLbl {
    if (!_accountLbl) {
        _accountLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _accountLbl.font = FONT_PR(13 );
        _accountLbl.textColor = QCWhite;
        _accountLbl.text = [[QCSingleton instance].account encryptedPhone];
        [_accountLbl sizeToFit];
    }
    return _accountLbl;
}

- (UILabel *)accountDescLbl {
    if (!_accountDescLbl) {
        _accountDescLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _accountDescLbl.font = FONT_PR(13 );
        _accountDescLbl.textColor = QCWhite;
        _accountDescLbl.text = @"用户";
        [_accountDescLbl sizeToFit];
    }
    return _accountDescLbl;
}

@end
