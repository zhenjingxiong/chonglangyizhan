//
//  ConsultInfoModel.h
//  QCAppProject
//
//  Created by yjl on 2021/8/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ConsultInfoModel : NSObject

@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *createDate;
@property (nonatomic, strong) NSString *createDateStr;
@property (nonatomic, strong) NSString *handleDate;
@property (nonatomic, strong) NSString *handleDateStr;
@property (nonatomic, strong) NSString *handleUserId;
@property (nonatomic, strong) NSString *handleUserName;
@property (nonatomic, strong) NSString *cId;
@property (nonatomic, assign) NSInteger jsonUpdateFlag;
@property (nonatomic, assign) NSInteger mybatisRecordCount;

@property (nonatomic, strong) NSString *openid;
@property (nonatomic, strong) NSString *orderNo;
@property (nonatomic, strong) NSString *remark;
@property (nonatomic, strong) NSString *reply;

@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *userPhone;
@property (nonatomic, strong) NSString *workerNo;
@property (nonatomic, assign) NSInteger  wstatus;
@property (nonatomic, strong) NSString *wstatusStr;

@end

NS_ASSUME_NONNULL_END
