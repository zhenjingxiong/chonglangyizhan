//
//  ConsultTopView.m
//  QCAppProject
//
//  Created by yjl on 2021/8/17.
//

#import "ConsultTopView.h"

@interface ConsultTopView ()

@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIView *noticeView;
@end

@implementation ConsultTopView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self == [super initWithFrame:frame]) {
        [self initConstraints];
    }
    return self;
}

- (void)initConstraints {
    [self addSubview:self.bgView];
    
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:self.bgView.frame];
    imageview.image = fImageNamed(@"home_bg");
    [self.bgView addSubview:imageview];
    [self.bgView addSubview:self.noticeView];
}


- (void)centerButton:(UIButton *)button{
    button.backgroundColor = [UIColor clearColor];
    CGSize imageSize = button.imageView.frame.size;
    CGSize titleSize = button.titleLabel.frame.size;
    
    button.titleEdgeInsets = UIEdgeInsetsMake(8, -imageSize.width, -imageSize.height -5, 0);
    button.imageEdgeInsets = UIEdgeInsetsMake(-titleSize.height -5, 0, 0, -titleSize.width);
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 278)];
    }
    return _bgView;
}

- (UIView *)noticeView {
    if (!_noticeView) {
        _noticeView = [[UIView alloc] initWithFrame:CGRectMake(16, kStatusBarHeight+40, SCREEN_WIDTH - 32, 24)];
        _noticeView.backgroundColor = kHexColorAlpha(@"#FFECDD", 0.3);;
        _noticeView.layer.cornerRadius = 12.0f;
        UIImageView *noticeImage = [[UIImageView alloc] initWithImage:fImageNamed(@"home_notice")];
        noticeImage.frame =CGRectMake(10, 5, 12, 14);
        [_noticeView addSubview:noticeImage];
        UILabel *titleLbl = [[UILabel alloc] initWithFrame:self.noticeView.bounds];
        titleLbl.font = FONT_PR(11);
        titleLbl.text = @"您的问题我们正在积极处理中哦～";
        titleLbl.numberOfLines = 1;
        titleLbl.textColor = QCWhite;
        titleLbl.left = noticeImage.right + 8;
        titleLbl.width = _noticeView.width - 40;
        [_noticeView addSubview:titleLbl];
    }
    return _noticeView;
}


@end
