//
//  ConsultTableViewCell.m
//  QCAppProject
//
//  Created by yjl on 2021/8/18.
//

#import "ConsultTableViewCell.h"
#import "ConsultInfoModel.h"

@interface ConsultTableViewCell ()

@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UILabel *timeLbl;
@property (nonatomic, strong) UIView *horLine;
@property (nonatomic, strong) UIImageView *arrowImageView;

@end

@implementation ConsultTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self == [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initConstraints];
    }
    return self;
}

- (void)configCellData:(id)sender {
    if (![sender isKindOfClass:[ConsultInfoModel class]]) {
        return;
    }
    ConsultInfoModel *model = sender;
    self.titleLbl.text = model.content;
    self.timeLbl.text = model.createDateStr;
}


- (void)initConstraints {
    UIView *superView = self.contentView;
    [superView addSubview:self.containerView];
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left);
        make.right.equalTo(superView.mas_right);
        make.top.equalTo(superView.mas_top);
        make.bottom.equalTo(superView.mas_bottom);
    }];
    
    [self.containerView addSubview:self.titleLbl];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@21);
        make.top.equalTo(@22);
        make.width.equalTo(@(SCREEN_WIDTH - 42 - 24));
    }];
    
    [self.containerView addSubview:self.timeLbl];
    [self.timeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLbl.mas_left);
        make.top.equalTo(self.titleLbl.mas_bottom).offset(8);
    }];
    
    [self.containerView addSubview:self.arrowImageView];
    [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@-24);
        make.centerY.equalTo(self.containerView.mas_centerY);
        make.size.equalTo(@(CGSizeMake(5, 8)));
    }];
    
    [self.containerView addSubview:self.horLine];
    [self.horLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLbl.mas_left);
        make.right.equalTo(self.titleLbl.mas_right);
        make.top.equalTo(self.timeLbl.mas_bottom).offset(22);
        make.height.equalTo(@(0.6));
    }];
    
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.horLine.mas_bottom);
    }];
}

- (UIView *)containerView {
    if (!_containerView) {
        _containerView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return _containerView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _titleLbl.font = FONT_PR(15);
        _titleLbl.textColor = QCBlack;
//        _titleLbl.text = @"设备突然使用不了......";
        _titleLbl.numberOfLines = 0;
        [_titleLbl sizeToFit];//自适应，在这可写可不写，一般在frame布局时，设置width没有设置高度，调用此方法可自动适应高度
    }
    return _titleLbl;
}

- (UILabel *)timeLbl {
    if (!_timeLbl) {
        _timeLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _timeLbl.font = FONT_PR(12);
        _timeLbl.textColor = QCSubBlack;
//        _timeLbl.text = @"2020-09-08 12:00";
        [_timeLbl sizeToFit];
    }
    return _timeLbl;
}

- (UIView *)horLine {
    if (!_horLine) {
        _horLine = [[UIView alloc] initWithFrame:CGRectZero];
        _horLine.backgroundColor = QCLineColor;
    }
    return _horLine;
}

- (UIImageView *)arrowImageView {
    if (!_arrowImageView) {
        _arrowImageView = [[UIImageView alloc] initWithImage:fImageNamed(@"arrow_icon")];
    }
    return _arrowImageView;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
