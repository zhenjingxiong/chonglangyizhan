//
//  ConsultTableViewCell.h
//  QCAppProject
//
//  Created by yjl on 2021/8/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ConsultTableViewCell : UITableViewCell

- (void)configCellData:(id)sender;

@end

NS_ASSUME_NONNULL_END
