//
//  SceneDelegate.h
//  QCAppProject
//
//  Created by yjl on 2021/8/13.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

