//
//  NSString+Ext.h
//  QieyouVisitorKit
//
//  Created by 李赛强 on 15/3/31.
//  Copyright (c) 2015年 lisaiqiang. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString (Ext)

- (BOOL)isBlank;
- (BOOL)isValid;
- (NSString *)removeWhiteSpacesFromString;


- (NSUInteger)countNumberOfWords;
- (BOOL)containsString:(NSString *)subString;
- (BOOL)isBeginsWith:(NSString *)string;
- (BOOL)isEndssWith:(NSString *)string;

- (NSString *)replaceCharcter:(NSString *)olderChar withCharcter:(NSString *)newerChar;
- (NSString*)getSubstringFrom:(NSInteger)begin to:(NSInteger)end;
- (NSString *)addString:(NSString *)string;
- (NSString *)removeSubString:(NSString *)subString;

- (BOOL)containsOnlyLetters;
- (BOOL)containsOnlyNumbers;
- (BOOL)containsOnlyNumbersAndLetters;
- (BOOL)isInThisarray:(NSArray*)array;

+ (NSString *)getStringFromArray:(NSArray *)array;
+ (NSString *)getUrlStringFromArray:(NSArray *)array;
- (NSArray *)getArray;

+ (NSString *)getMyApplicationVersion;
+ (NSString *)getMyApplicationName;

- (NSData *)convertToData;
+ (NSString *)getStringFromData:(NSData *)data;

- (BOOL)isValidEmail;
- (BOOL)isVAlidPhoneNumber;
- (BOOL)isMobileNumber;
- (BOOL)isValidUrl;

- (NSString *)md5;
+ (NSString *)hexStringFromString:(NSString *)string;


+(BOOL) isValidateMobile:(NSString *)mobile ;

//判断是否为整形：
+ (BOOL)isPureInt:(NSString*)string;

//判断是否为浮点形：
+ (BOOL)isPureFloat:(NSString*)string;

#pragma mark - 身份证识别
+ (BOOL) validateIdentityCard: (NSString *)identityCard;

+ (NSString *)timeDateFormatter:(NSDate *)date type:(int)type;

+(NSString *)toLower:(NSString *)str;//字符串转为小写字母
+(NSString *)toUpper:(NSString *)str;//字符串转为大写字母

#pragma mark - 计算显示字符串的宽度或高度

/**
 abstact:根据文本内容获取其显示大小
 textInfo:文本内容
 fontSize:字体大小
 width:所在view的宽度
 */
+ (CGSize)getSizeOfTextInfo:(NSString *)textInfo andFontSize:(CGFloat)fontSize andWidth:(CGFloat)width;


+ (CGSize)getSizeOfTextInfo:(NSString *)textInfo andFont:(UIFont *)font andWidth:(CGFloat)width;

/**
 * 动态返回字符串的高度
 *  X 为固定的宽和高
 * size 计算宽CGSizeMake(MAXFLOAT, X) 计算高CGSizeMake(X, MAXFLOAT)
 */
- (CGSize)sizeWithMyFont:(UIFont *)font;

- (CGSize)sizeWithMyFont:(UIFont *)font size:(CGSize)size;

+ (BOOL)judgeIdentityStringValid:(NSString *)identityString;

- (NSMutableAttributedString *)addAttributeWithColor:(UIColor *)textColor forRange:(NSRange)rang;
- (NSMutableAttributedString *)addAttributeWithFont:(CGFloat)textFont andTextColor:(UIColor *)textColor forRange:(NSRange)rang;

+ (NSString *)ossSizeWithOriginStr:(NSString *)originStr withWidth:(NSInteger)width;

+ (NSString *)plistVersion;

- (NSString *)encryptedPhone;

+ (NSString *)md5EncryptRandomNumber;
@end
