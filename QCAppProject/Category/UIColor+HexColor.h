//
//  UIColor+HexColor.h
//  QCAppProject
//
//  Created by yjl on 2021/8/16.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (HexColor)


+ (UIColor *)colorForHexString:(NSString *)colorCode;

+ (UIColor *)colorForHexString:(NSString *)colorCode Alpha:(CGFloat)alpha;

// UIColor转#ffffff格式的字符串
+ (NSString *)hexStringFromColor:(UIColor *)color;

@end

NS_ASSUME_NONNULL_END
