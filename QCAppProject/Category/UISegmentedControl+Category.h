//
//  UISegmentedControl+Category.h
//  QCAppProject
//
//  Created by yjl on 2021/8/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UISegmentedControl (Category)

- (void)segmentedIOS13Style;

//- (void)setTintColor:(UIColor *)tintColor;

@end

NS_ASSUME_NONNULL_END
