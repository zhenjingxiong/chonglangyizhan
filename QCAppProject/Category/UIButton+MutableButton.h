//
//  UIButton+MutableButton.h
//  QCAppProject
//
//  Created by yjl on 2021/8/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (MutableButton)


typedef void(^ISLimitWidth)(BOOL yesORNo, id data);

///**
// *  根据添加的title 改变 button 的长度
// *
// *  @param text
// */
//
//- (void)setMutableTitleWithString:(NSString *)text textFont:(UIFont *)textFont forState:(UIControlState)UIControlState;

+ (float)btnLimitWidth:(float)cellWidth isLimitWidth:(ISLimitWidth)isLimitWidth;
+ (float)btnWidthText:(NSString *)text;
//左文 右图
- (void)resetEdgeInsetWithBtn:(UIButton *)btn image:(UIImage *)image text:(NSString *)text;
//左文 下图
+ (void)resetEdgeInsetWithUpDownBtn:(UIButton *)btn;

//左文 右图
- (void)resetLeftTextRightImageWithBtn:(UIButton *)btn image:(UIImage *)image font:(UIFont *)font;

@end

NS_ASSUME_NONNULL_END
