//
//  UIColor+HexColor.m
//  QCAppProject
//
//  Created by yjl on 2021/8/16.
//

#import "UIColor+HexColor.h"

@implementation UIColor (HexColor)

+ (UIColor *)colorForHexString:(NSString *)colorCode {
    NSMutableString *string = [NSMutableString stringWithString:colorCode];
    [string replaceOccurrencesOfString:@"#" withString:@"" options:0 range:NSMakeRange(0, [string length])];
    
    // Check if named color exists
    NSString *colorName = [NSString stringWithFormat:@"%@Color", [string lowercaseString]];
    SEL selector = NSSelectorFromString(colorName);
    if ([[UIColor class] respondsToSelector:selector]) {
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[[UIColor class] methodSignatureForSelector:selector]];
        [invocation setSelector:selector];
        [invocation setTarget:[UIColor class]];
        [invocation invoke];
        UIColor *result;
        [invocation getReturnValue:&result];
        
        return result;
    }
    
    // Check if size is enough
    NSUInteger length = [string length];
    switch (length) {
        case 1:
            // The pattern is easy to form
            [string appendFormat:@"%@%@%@%@%@", string, string, string, string, string];
            break;
        case 2:
            // Once again, repeat the pattern
            [string appendFormat:@"%@%@", string, string];
            break;
        case 3:
            // And again, repeat the pattern
            [string appendFormat:@"%@", string];
            break;
        case 4:
            // Now it's a bit more difficult, repeat, but then cut the end off
            [string appendString:[string substringToIndex: 2]];
            break;
        case 5:
            // Same as with four, but add one less
            [string appendString:[string substringToIndex: 1]];
            break;
        default:
            break;
    }
    
    // Storage for all the values
    unsigned color;
    
    // Now we can proceed to calculate the values, start by creating a range of the string to look at
    [[NSScanner scannerWithString:string] scanHexInt:&color]; // Grabs color value
    
    return [UIColor colorWithRed:(CGFloat)(((color >> 16) & 0xFF) / 255.f)
                           green:(CGFloat)(((color >> 8) & 0xFF) / 255.f)
                            blue:(CGFloat)((color & 0xFF) / 255.f)
                           alpha:1.f];
}



+ (UIColor *)colorForHexString:(NSString *)colorCode Alpha:(CGFloat)alpha{
    NSMutableString *string = [NSMutableString stringWithString:colorCode];
    [string replaceOccurrencesOfString:@"#" withString:@"" options:0 range:NSMakeRange(0, [string length])];
    
    // Check if named color exists
    NSString *colorName = [NSString stringWithFormat:@"%@Color", [string lowercaseString]];
    SEL selector = NSSelectorFromString(colorName);
    if ([[UIColor class] respondsToSelector:selector]) {
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[[UIColor class] methodSignatureForSelector:selector]];
        [invocation setSelector:selector];
        [invocation setTarget:[UIColor class]];
        [invocation invoke];
        UIColor *result;
        [invocation getReturnValue:&result];
        
        return result;
    }
    
    // Check if size is enough
    NSUInteger length = [string length];
    switch (length) {
        case 1:
            // The pattern is easy to form
            [string appendFormat:@"%@%@%@%@%@", string, string, string, string, string];
            break;
        case 2:
            // Once again, repeat the pattern
            [string appendFormat:@"%@%@", string, string];
            break;
        case 3:
            // And again, repeat the pattern
            [string appendFormat:@"%@", string];
            break;
        case 4:
            // Now it's a bit more difficult, repeat, but then cut the end off
            [string appendString:[string substringToIndex: 2]];
            break;
        case 5:
            // Same as with four, but add one less
            [string appendString:[string substringToIndex: 1]];
            break;
        default:
            break;
    }
    
    // Storage for all the values
    unsigned color;
    
    // Now we can proceed to calculate the values, start by creating a range of the string to look at
    [[NSScanner scannerWithString:string] scanHexInt:&color]; // Grabs color value
    
    return [UIColor colorWithRed:(CGFloat)(((color >> 16) & 0xFF) / 255.f)
                           green:(CGFloat)(((color >> 8) & 0xFF) / 255.f)
                            blue:(CGFloat)((color & 0xFF) / 255.f)
                           alpha:alpha];
}

// UIColor转#ffffff格式的字符串
+ (NSString *)hexStringFromColor:(UIColor *)color {
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    
    CGFloat r = components[0];
    CGFloat g = components[1];
    CGFloat b = components[2];
    
    return [NSString stringWithFormat:@"#%02lX%02lX%02lX",
            lroundf(r * 255),
            lroundf(g * 255),
            lroundf(b * 255)];
}

@end
