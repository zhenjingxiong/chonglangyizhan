//
//  UIButton+MutableButton.m
//  QCAppProject
//
//  Created by yjl on 2021/8/17.
//

#import "UIButton+MutableButton.h"
#import "NSString+Ext.h"

@implementation UIButton (MutableButton)

- (void)resetEdgeInsetWithBtn:(UIButton *)btn image:(UIImage *)image text:(NSString *)text{
   
   CGFloat btnWidth = [UIButton btnWidthText:text];

   [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -image.size.width, 0, image.size.width)];
   [btn setImageEdgeInsets:UIEdgeInsetsMake(0, btnWidth - image.size.width - 10, 0,0)];
}

- (void)resetLeftTextRightImageWithBtn:(UIButton *)button image:(UIImage *)image font:(UIFont *)font {
   CGFloat titleWid = [NSString getSizeOfTextInfo:button.currentTitle andFont:font andWidth:button.frame.size.width].width;
   [button setTitleEdgeInsets:UIEdgeInsetsMake(0, -image.size.width, 0, image.size.width)];
   [button setImageEdgeInsets:UIEdgeInsetsMake(0, titleWid, 0, -titleWid)];
}

+ (void)resetEdgeInsetWithUpDownBtn:(UIButton *)btn {
   CGSize imgViewSize,titleSize,btnSize;
   UIEdgeInsets imageViewEdge,titleEdge;
   CGFloat heightSpace = 10.0f;
   
   
   //设置按钮内边距
   
   imgViewSize = btn.imageView.bounds.size;
   
   titleSize = btn.titleLabel.bounds.size;
   
   btnSize = btn.bounds.size;//17 21
   //10 0
   //top 10  left 0 bottom 40 right -48
//    imageViewEdge = UIEdgeInsetsMake(heightSpace,0.0, btnSize.height -imgViewSize.height - heightSpace, - titleSize.width);
   //10 0
   //top 10  left 0 bottom 40 right -48
   if (btn.tag == 101) {
       imageViewEdge = UIEdgeInsetsMake(heightSpace,(btnSize.width - imgViewSize.width)/2 - heightSpace, btnSize.height -imgViewSize.height - heightSpace, 0);
   } else {
       imageViewEdge = UIEdgeInsetsMake(heightSpace,0.0, btnSize.height -imgViewSize.height - heightSpace, - titleSize.width);
   }
   [btn setImageEdgeInsets:imageViewEdge];
   
   titleEdge = UIEdgeInsetsMake(imgViewSize.height + heightSpace, - imgViewSize.width, 0.0, 0.0);
   
   [btn setTitleEdgeInsets:titleEdge];
}

+ (float)btnLimitWidth:(float)cellWidth isLimitWidth:(ISLimitWidth)isLimitWidth {
   float limitWidth = SCREEN_WIDTH - 16 -16;
   if (cellWidth >= limitWidth) {
       cellWidth = limitWidth;
       isLimitWidth ? isLimitWidth(YES, @(cellWidth)) : nil;
       return cellWidth;
   }
   isLimitWidth ? isLimitWidth(NO, @(cellWidth)) : nil;
   return cellWidth;
   
}


+ (float)btnWidthText:(NSString *)text {
   float btnWidth;
   CGSize size = [text sizeWithAttributes:
                  @{NSFontAttributeName:
                        FONT_PR(14)}];
   
   btnWidth = [self btnLimitWidth:ceilf(size.width + 20 + 30) isLimitWidth:nil];
   return btnWidth;
}

@end
