//
//  QCLoginViewController.h
//  QCAppProject
//
//  Created by yjl on 2021/8/19.
//

#import "QCBaseViewController.h"


typedef void(^LoginSuccessBlock)(void);

NS_ASSUME_NONNULL_BEGIN

@interface QCLoginViewController : QCBaseViewController

@property (nonatomic, copy) LoginSuccessBlock loginSuccessBlock;

@end

NS_ASSUME_NONNULL_END
