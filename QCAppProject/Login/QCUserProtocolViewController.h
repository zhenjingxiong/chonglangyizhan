//
//  QCUserProtocolViewController.h
//  QCAppProject
//
//  Created by jiale yang on 2021/8/21.
//

#import "QCBaseViewController.h"

typedef void(^ProtocolJump)(void);

NS_ASSUME_NONNULL_BEGIN

@interface QCUserProtocolViewController : QCBaseViewController

@property (nonatomic, copy) ProtocolJump isProtocolJump;

@end

NS_ASSUME_NONNULL_END
