//
//  AppColorConfig.h
//  QCAppProject
//
//  Created by yjl on 2021/8/16.
//

#ifndef AppColorConfig_h
#define AppColorConfig_h


#define kHexColor(HexString) [UIColor colorForHexString:HexString]
#define kHexColorAlpha(HexString,a) [UIColor colorForHexString:HexString Alpha:(a)]
//颜色规范

#define QCBlack     kHexColor(@"333333")  //主体黑色
#define QCSubBlack  kHexColor(@"666666")  //二级黑
#define QCDesBlack  kHexColor(@"8E8888")  //三级黑
#define QCTipBlack  kHexColor(@"CCCCCC")  //其他黑
#define QCLineColor kHexColor(@"EFEDED")  //线的颜色
#define QCBgColor   kHexColor(@"EEEEEE")  //背景色
#define QCRedColor  kHexColor(@"F85951")  //主体红色
#define QCWhite     kHexColor(@"FFFFFF")  //主体白色
#define QCOrange    kHexColor(@"FF5403")  //主体橙色


#endif /* AppColorConfig_h */
