//
//  AppConfig.h
//  QCAppProject
//
//  Created by yjl on 2021/8/16.
//

#ifndef AppConfig_h
#define AppConfig_h


//-------------------   宏 -------------------------
//弱引用
#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;

#define   QCWindow    [UIApplication sharedApplication].delegate.window

#define   QCBottom   if (@available(iOS 11.0, *)) { return  QCWindow.safeAreaInsets.bottom; } else { return 0;}

//#if DEBUG
//#define NSLog(FORMAT, ...) fprintf(stderr,"\nfunction:%s Line:%d Content:%s\n", __FUNCTION__, __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String])
//#else
//#define NSLog(FORMAT, ...) nil
//#endif

//日志
#ifdef DEBUG
#define QCLog(fmt, ...)      NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define NSLog(fmt, ...)     {}
#define QCLog(fmt, ...)     {}
#endif

#define fImageNamed(name)  [UIImage imageNamed:name]


//-------------------    公共变量配置 -------------------------


//注册协议
#define REGISTER_AGREEMENT_URL   @"https://mapi2.fadada.com/useagreement.html"
//隐私协议
#define PRIVACY_AGREEMENT_URL     @"https://mapi2.fadada.com/privacy.html"


//-------------------  通知  -------------------------



//------------------- 机型定义 -------------------------
#define iPhone4s ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)

#define iPhone5s ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#define iPhone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)
#define iPhone6Plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size) : NO)
#define KiPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)
#define KiPhoneXr ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) : NO)
#define KiPhoneXR ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1624), [[UIScreen mainScreen] currentMode].size) : NO)
#define KiPhoneXs ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)
#define KiPhoneXs_Max ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size) : NO)

//#define isIphoneX_all (KiPhoneX || KiPhoneXr || KiPhoneXR || KiPhoneXs || KiPhoneXs_Max)

#define isIphoneX_all isIPhoneX

#define isIPhoneX [[UIScreen mainScreen] bounds].size.width >=375.0f && [[UIScreen mainScreen] bounds].size.height >=812.0f&& IS_IPHONE
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

// 检查数据类型是否匹配+指针是否为空+内容是否为空
#define QC_IS_STR_NIL(objStr) (![objStr isKindOfClass:[NSString class]] || nil == objStr || [objStr length] <= 0)
#define QC_IS_DICT_NIL(objDict) (![objDict isKindOfClass:[NSDictionary class]] || nil == objDict || [objDict count] <= 0)
#define QC_IS_ARRAY_NIL(objArray) (![objArray isKindOfClass:[NSArray class]] || nil == objArray || [objArray count] <= 0)
#define QC_IS_NUM_NIL(objNum) (![objNum isKindOfClass:[NSNumber class]])


//-------------------   尺寸   -------------------------


//屏幕大小
#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

//适配比例
#define SCREEN_WIDTH_SCALE ([UIScreen mainScreen].bounds.size.width/375.)
#define SCREEN_HEIGHT_SCALE ([UIScreen mainScreen].bounds.size.height/667.)

//适配
#define AD_W(S) (isIPhoneX ? (S * (375/320.)) : S )
#define AD_H(S) (isIPhoneX ? (S * (667/568.)) : S )



//-------------------- 界面宽高 -----------------------------
//状态栏高度
#define kStatusBarHeight (isIPhoneX ? 44 : 20)

//控制器跳转后超出屏幕问题 垂直偏移量
#define VERTICAL_OFFSET 64

//底部安全距离
#define kBottomHeight  (isIPhoneX ? 34:0)

//顶部导航和状态栏在iphonex上的高度
#define kTopBarHeight  (isIPhoneX ? 88:64)

// TabBar高度
#define kTabBarHeight (CGFloat)(isIPhoneX?(49+34):(49))
// 底部安全区域远离高度
#define kBottomSafeHeight (CGFloat)(isIPhoneX?(34):(0))



#define kMD5RandomKey @"qcwulian_kMD5RandomKey"

#define kQueryLinkKey @"qcwulian_kQueryLinkKey"

#define kJpushRegistrationID @"qcwulian_kJpushRegistrationID"

//-------------------- 字体 -----------------------------
//系统字体定义
#define FONT(F) [UIScreen mainScreen].bounds.size.width > 375?[UIFont systemFontOfSize:(F+1)]:[UIFont systemFontOfSize:F]

static inline UIFont *QCFont(CGFloat F) {
    return [UIScreen mainScreen].bounds.size.width > 375?[UIFont systemFontOfSize:(F+1)]:[UIFont systemFontOfSize:F];
}


//PingFangSC-Medium 字体定义

//苹方-简 中黑体
#define FONT_PM(F) [UIScreen mainScreen].bounds.size.width > 375?[UIFont systemFontOfSize:F+1 weight:UIFontWeightMedium]:[UIFont systemFontOfSize:F weight:UIFontWeightMedium]

//苹方-简 常规体
#define FONT_PR(F) [UIScreen mainScreen].bounds.size.width > 375?[UIFont systemFontOfSize:F+1 weight:UIFontWeightRegular]:[UIFont systemFontOfSize:F weight:UIFontWeightRegular]

//苹方-简 中粗体
#define FONT_PS(F) [UIScreen mainScreen].bounds.size.width > 375?[UIFont systemFontOfSize:F+1 weight:UIFontWeightSemibold]:[UIFont systemFontOfSize:F weight:UIFontWeightSemibold]

//苹方-简 粗体
#define FONT_PB(F) [UIScreen mainScreen].bounds.size.width > 375?[UIFont systemFontOfSize:F+1 weight:UIFontWeightBold]:[UIFont systemFontOfSize:F weight:UIFontWeightBold]

//苹方-简 特粗体
#define FONT_PH(F) [UIScreen mainScreen].bounds.size.width > 375?[UIFont fontWithName:@"PingFangSC-Heavy" size:F+1]:[UIFont fontWithName:@"PingFangSC-Heavy" size:F]

//RGBCOLOR
#define UIColorFromHexValue(hexValue)       [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16))/255.0 green:((float)((hexValue & 0xFF00) >> 8))/255.0 blue:((float)(hexValue & 0xFF))/255.0 alpha:1.0]
#define rgbColor(r, g, b)                   [UIColor colorWithRed:(r) / 255.0f green:(g) / 255.0f blue:(b) / 255.0f alpha:1]
#define rgbaColor(r, g, b, a)               [UIColor colorWithRed:(r) / 255.0f green:(g) / 255.0f blue:(b) / 255.0f alpha:(a)]



#define UD [NSUserDefaults standardUserDefaults]

//token过期码
#define TokenExpCode 113652

//long token过期码
#define LongTokenExpCode 113672

//找回密码临时token
#define FPwdTokenExpCode 1136187

//第一次使用app的标记（本地）
#define kFirstUseAppMark @"kFirstUseAppMark_ios"


#define showAlert(x,target){UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:x preferredStyle:UIAlertControllerStyleAlert];[alertController addAction:[UIAlertAction actionWithTitle:@"我知道了" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}]];[target presentViewController:alertController animated:YES completion:nil];};


typedef enum : NSUInteger {
    OrderStatusDefault = 0,
    OrderStatusWaiting = 1,
    OrderStatusDealled = 2
} OrderStatus;

#endif /* AppConfig_h */
